# Logic Tensor Network on Data Base

[Excerpts from Lucas Benchberger](https://lucas-bechberger.de/2017/11/16/what-are-logic-tensor-networks/)

## Data Base Fixing

* Data could be corrupted, typical cases are accidental mispelling typos in form data entry.

## Logic Tensor Network

Logic Tensor Networks are an ongoing research project.
Logic Tensor Networks provide a nice way of combining neural networks with symbolic rules by using fuzzy logic.
One of the current challenges in AI is to combine explicit, symbolic knowledge in the form of rules (like “red apples are sweet”) with implicit, subsymbolic knowledge like the weights of a neural network (e.g., of a neural network that can decide whether there is an apple in an image or not). Logic Tensor Networks (or LTNs, for short) gives us a way to combine both types of knowledge with each other.

### Logic Constrains

Every concept is represented by a logical predicate. For instance, the concept “apple” is written as a function “apple(x)” which takes as an argument any object x and returns true if this object is an apple and false if it is not. Abstract rules like “red apples are sweet” can then be formulated as logical formulas involving these predicates:
∀x: apple(x) ∧ red(x) → sweet(x)
This statement can be interpreted as follows: “For all possible objects, the following is true: If the object is both and apple and red, then this object must also be sweet.”In other words: all red apples are sweet.

### Fuzzy Logic Constraints

In fuzzy logic, you assume that statements don’t have to be either completely true or completely false. Instead, you allow for statements to be partially true by defining a “degree of truth” – usually a number between 0 and 1 (where 0 corresponds to “completely false” and 1 corresponds to “completely true”). Applying this to our concepts/predicates, we get a degree of membership, similarly to what I proposed in my formalization of conceptual spaces. So apple(x) will now return a number between 0 and 1 which indicates how much the object x can be considered to be an apple. This way, we can express imprecise knowledge and imprecise concept boundaries.

Of course, one can also combine multiple fuzzy truth values with each other. For instance, let’s say that apple(x) = 0.8 and red(x) = 0.5 for some object x. Now the question is: What is the degree of truth for a formula like apple(x) ∧ red(x)? The most common definition is to just take the minimum of both individual truth values. That is, the degree of truth for apple(x) ∧ red(x) would be the minimum of apple(x) = 0.8 and red(x) = 0.5, which is 0.5. Similarly, one can also define degrees of truth for the operations of disjunction, conjunction, implication, etc.

* First Order Logic constrained with constant in space R[0,1]
* Axioms
* Constants

e.g.

### Neural Network

Every one of our concepts/predicates is now represented by a neural network and objects are represented by points in a feature space. The input to the neural network for “apple” is a point in the feature space and its output is a number between 0 and 1 – the degree of membership of this object to the “apple” concept! The neural network is therefore used to learn the membership function for the given concept. The underlying feature space can be based on features extracted from images or other sensory data. 

## Two Hot Points

CONSTRAINED CLASSIFY

we now just found a way to connect subsymbolic information (weights in a neural network that classifies for instance images) with symbolic logical information (like the rule “all red apples are sweet”). Whenever we observe a new object (e.g., based on an image), we can use the neural networks to classify it, and then we can use our formulas to do reasoning on it. That’s quite nice.

Moreover, we can use symbolic rules to guide the process of learning the neural networks’ weights. We can optimize the weights of the “apple” network not only such that it correctly classify images as being apples, but also such that the fuzzy formula apple(x) ∧ red(x) → sweet(x) is true for all points x in our feature space. The symbolic rules can therefore give us additional constraints for the classification.

DERIVE RULES

Furthermore, after we have learned the weights of our neural networks, we can also check whether other rules that we didn’t use during the learning process (like “apple(x) ∧ green(x) → sour(x)“) are true. So we can query our networks to find additional rules that we didn’t have before.


## Details

* We are using the simplest net: single neuron per feature.
The embedding has to be guessed betwwen under and over fitting the ginven constants.

* mebership function: quadratic function, so it has the shape of a parabola.

* train, LTNs are neural networks trained by gradient descent and as the number of hyperparameters to tune is quite large.

* LTNs currently assume that the Euclidean metric is used everywhere in the feature space.

### Fixing in LTNW

we now just found a way to connect subsymbolic information (weights in a neural network that classifies for instance images) with symbolic logical information (like the rule “all red apples are sweet”). Whenever we observe a new object (e.g., based on an image), we can use the neural networks to classify it, and then we can use our formulas to do reasoning on it. That’s quite nice.
Moreover, we can use symbolic rules to guide the process of learning the neural networks’ weights. We can optimize the weights of the “apple” network not only such that it correctly classify images as being apples, but also such that the fuzzy formula apple(x) ∧ red(x) → sweet(x) is true for all points x in our feature space. The symbolic rules can therefore give us additional constraints for the classification.
Furthermore, after we have learned the weights of our neural networks, we can also check whether other rules that we didn’t use during the learning process (like “apple(x) ∧ green(x) → sour(x)“) are true. So we can query our networks to find additional rules that we didn’t have before.


* Hypotesis 1 : some record have a few corrupted columns, compared to columns cardinality.
    => Entity identity is preserved
* Hypotesis 2 : a few record have corruptec values, compared to row cardinality.
    => LTN non si usa per errori sistematici
* Hypotesis 3 : values in columns are categorizable.

Scenario tipo: data entry con alta ripetizione di valori.

* Value fixing steps: 
0. given an axiomatized database
1. remove axioms contraining the value cell to fix.
2. train the network
3. ask the net the values computed that best result that will fit the constraints.

---
Estensioni:

Real domains, imply a total order on values,
Categories may obtain a partial order by value frequency cardinality 


Dictionaries
attrib = [correct values]
calcola la distanza. es "yxs" per "yes" per testi, 


Outliers
Frequency for feature value
attrib.value=freq


Prefix
autofix x distance dal dizionario

Fix
select column caratteristiche di identita'
x cardinality
x vincoli interentita'

--------------

(chiave)(caratteristiche-di-identita')(extra)
[(attributo-chiave-parziale)][(attributo-non-chiave-caratteristico)]
hospital.csv contiene due entita' in outerjoin

chiave puo' essere eliminata
la caratteristica di identita'