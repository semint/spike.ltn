# DB QUALITY

## Forme normali

https://it.wikipedia.org/wiki/Normalizzazione_(informatica)

1.
Definizione: Si dice che una base dati è in 1NF (prima forma normale) se vale la seguente relazione: per ogni relazione contenuta nella base dati, una relazione è in 1NF se e solo se:

    Ciascun attributo è definito su un dominio con valori atomici (indivisibili);
    Ogni attributo contiene un singolo valore da quel dominio.

2.
Definizione: Una base dati è invece in 2NF (seconda forma normale) quando è in 1NF e per ogni relazione tutti gli attributi non-chiave dipendono funzionalmente dall'intera chiave composta (ovvero la relazione non ha attributi che dipendono funzionalmente da una parte della chiave). 

3.
Definizione: Una base dati è in 3NF (terza forma normale) se è in 2NF e se tutti gli attributi non-chiave dipendono dalla chiave soltanto, ossia non esistono attributi non-chiave che dipendono da altri attributi non-chiave. Tale normalizzazione elimina la dipendenza transitiva degli attributi dalla chiave. 

https://it.wikipedia.org/wiki/Data_cleaning

Qualita' dei dati

I criteri qualitativi che deve rispettare una bonifica di un sistema informativo, sono:

    Accuratezza
    Integrità
    Completezza
    Validità
    Consistenza
    Uniformità
    Densità
    Unicità

Unificazione dello schema
Errori atomici

approccio schema level
1. si cerca d stabilire somiglianz tra due strutture database, cercando di farne il merge.
2. verifica dei vincoli di integrita'


approccio instance level
si cercano metriche per individuare somiglianze e differenze tra i valori


----
es. due colonne Nome, CodiceFiscale
tra i due vi e' una dipendenza funzionale


https://it.wikipedia.org/wiki/Vincolo_di_integrit%C3%A0

ogni vincolo puo' essere visto come un predicato che puo' assumere il valore vero o falso.

vincoli interrelazionali (tra due o piu tab)
    vicolo 

vincoli intrarelazionali (una tab)
    chiave primaria
    chiave univoca
    vincolo di dominio