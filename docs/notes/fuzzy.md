
When is a database query result suprising? Data scientists implicitly ground quantitative results from data in qualitative models of the world. Suppose our database returns that Fred, a manager, earns more money than John. The data scientist might justify this result with a series of "soft" logical deductions: (1) Managers usually earn more than other employees, (2) Fred is a manager, (3) therefore it is not unsual that Fred earns more than John. These logical sentences sit in a middle ground between logic and probability called Modal Logic. Modal logic is a type of formal logic from the 1960s that extends classical logic to include operators expressing modality. A modal is a word that qualifies a statement (such as "usually")--more specifically, quantifies the fraction of possible worlds a statement is true.


https://github.com/sjyk/ar4ds/blob/master/docs/0-Introduction.md

On one extreme we have constructs based in pure prepositional or predicate logic (yes/no questions) and on the other extreme we have systems based in pure probabilistic logic (requires a complete model of the world).


Norma
Lukasiewicz t-norm
μ(a, b) = min(1, a+b),

Operatore Aggregatore
media armonica

Algoritmo di apprendimento
RMSProp



 In reallogic, instead, the level oftruth of a complex formula is determined by (fuzzy) logical reasoning, and the relationsbetween the features of different objects is learned through error minimization.

 Real Logic is open domain
CWA is not required, and existentially quantified formulas can be satisfied by “new individuals.

Real Logic: a uniform framework for learning and reasoning. Approximate satisfiability is defined as a learning task with both knowledge and data be-ing mapped onto real-valued vectors. With an inference-as-learning approach, relationalknowledge constraints and  state-of-the-art data-driven approaches can  be  integrated.

comparison with 
statistical relational learning, 
neural-symbolic computing, and 
(probabilistic) inductive logic programming approaches.


https://ece.uwaterloo.ca/~dwharder/Maplesque/FuzzySets/

The constructors Gamma ( Gamma ), L ( L ), Lambda ( Lambda ), and PI ( PI ) are named to suggest the shape of the resulting membership function.

