# cleaning data

* deduction
* induction
* garbage in garage out
* noise


        !pip install missingno > /dev/null
        import pandas as pd
        import missingno as msno
        import numpy as np

        url = "../data/titanic.csv"
        titanic = pd.read_csv(url)
        titanic.hist( layout=(2, 4));

        msno.matrix(titanic.sample(500));
        msno.bar(titanic.sample(500));
        msno.heatmap(titanic);
        pd.plotting.scatter_matrix(titanic, color='dimgray');

        # Compute the correlation matrix
        corr = titanic.corr()
        # Generate a mask for the upper triangle
        mask = np.zeros_like(corr, dtype=np.bool)
        mask[np.triu_indices_from(mask)] = True
        # Generate a custom diverging colormap
        cmap = sns.diverging_palette(220, 10, as_cmap=True)
        # Draw the heatmap with the mask and correct aspect ratio
        sns.heatmap(corr, mask=mask, cmap=cmap, vmax=0.8, vmin=-0.8, square=True);

        url = "../data/nypd-motor-vehicle-collisions.csv"
        collisions = pd.read_csv(url)

https://github.com/ResidentMario/missingno


categorici: ordinali (L,XL,XXL), nominali (F,M)
numerici: continui , interi

dati corrotti
dati assenti (nan potrebbe essere quello che cerchiamo)

    rimuovi riche o colonne
    introduci (impute) un valore medio, mediana, regression, random sampling
    no best way!! just try and choose what most make sense in context.

Pandas has a very handy function called dropna() that removes all rows that have at least one NaN.

        titanic.dropna().count()
        titanic.fillna(0).describe()
        titanic.fillna(titanic.mean()).describe()

regression

        titanic_dropped = titanic.dropna()
        X = titanic_dropped[['survived', 'fare']]
        y = titanic_dropped['age']
        mdl = sklearn.linear_model.LinearRegression().fit(X, y)


        titanic_X = titanic.dropna(subset = ['survived', 'fare']) # Inputs to an sklearn model can't have NaNs
        print(titanic_X.count())
        titanic_imputed = titanic_X.copy()
        titanic_imputed['age_imputed'] = mdl.predict(titanic_X[['survived', 'fare']])
        print(titanic_imputed.tail(10))
        print(titanic_imputed.count())
        titanic_fixed = titanic_X.copy()
        titanic_fixed['age'] = titanic_fixed['age'].fillna(titanic_imputed['age_imputed'])
        print(titanic_fixed.tail(10))
        print(titanic_fixed.count())

    
NOISE IN:
class (table)
feature (column)
observation (row)
labels 

cut data we don't need

    aggregation
        median
        average
    simple model
        smoothing
        normalization
    complex
        regression
        fitting
    dimensions reduction restoration
        wavelets FFT (frequency)
        encoding/enbedding (auto-encoder)

outliers, not expected data, could be:
    noise OR exceptional data

     detecting normal con un modello ed una soglia per definire cosa NON è normale.

    visualize data
    classification
    clustering
    regression/fitting + threashold (percentile?)

TRANSFORMING DATA
    normalizing data using a distribution
        chi quadro
        pareto

        # Distribution fitting
        y = data
        x_plot = scipy.linspace(np.min(y) - 3, np.max(y) + 3, 100)
        y_count, x = np.histogram(y, density=True)
        x = (x + np.roll(x, -1))[:-1] / 2.0
        h = sns.distplot(y, kde=False, norm_hist=True)

        dist_names = ['gamma', 'beta', 'rayleigh', 'norm', 'pareto', 'powernorm']

        for dist_name in dist_names:
            dist = getattr(scipy.stats, dist_name)
            params = dist.fit(y)
            # Separate parts of parameters
            arg = params[:-2]
            loc = params[-2]
            scale = params[-1]
            pdf_fitted = dist.pdf(x, loc=loc, scale=scale, *arg)
            sse = np.sum(np.power(y_count - pdf_fitted, 2.0))
            pdf_plot = dist.pdf(x_plot, loc=loc, scale=scale, *arg)
            sns.lineplot(x_plot, pdf_plot, label='{} ({:.5f})'.format(dist_name, sse))
        plt.legend(loc='upper right')
        ax = plt.gca()
        ax.set(title='Fitted Distributions (with SSE scores)',
               xlabel='data', ylabel='Normalised Count');


    for non normalized data
    first you can use a math (invertible) function to get data near normalized distribution
    box cox transofrmation


        url = "../data/titanic.csv"
        titanic = pd.read_csv(url)
        fare = titanic['fare']
        fare.apply(lambda x: np.log(x)).replace(-np.inf, 0).hist()

FIXING CATEGORICAL DATA
This is a classification example:
https://scikit-learn.org/stable/index.html

        np.random.seed(42)
        url = "../data/titanic.csv"
        titanic = pd.read_csv(url)
        titanic = titanic[['survived', 'fare', 'age']]
        titanic.dropna(inplace=True)
        titanic = titanic[(titanic[['fare', 'age']] != 0).all(axis=1)]  # Remove zero fares
        y = titanic['survived']
        X = titanic[['fare', 'age']]

        clf = sklearn.svm.LinearSVC(tol=1e-2, max_iter=10).fit(X, y)
        xx, yy = np.mgrid[0:600:1, 0:100:1]
        grid = np.c_[xx.ravel(), yy.ravel()]
        probs = clf.predict(grid).reshape(xx.shape)
        score = clf.score(X, y) * 100

        fig, (ax1, ax2) = plt.subplots(ncols=2)

        sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax1).set_title(
            "Titanic Survivors - SVM (10 iter) - {:.1f}%".format(score))
        ax1.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)

        X[:] = sklearn.preprocessing.MinMaxScaler().fit_transform(X[:])
        clf = sklearn.svm.LinearSVC(tol=1e-2, max_iter=10).fit(X, y)
        xx, yy = np.mgrid[0:1:0.001, 0:1:0.001]
        grid = np.c_[xx.ravel(), yy.ravel()]
        probs = clf.predict(grid).reshape(xx.shape)
        score = clf.score(X, y) * 100

        sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax2).set_title(
            "Titanic Survivors - SVM (10 iter, scaled) - {:.1f}%".format(score))
        ax2.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6);

    introduce an embedding
    one-hot encoding = new columns for each possible category
    feature hashing = from unbounded sets to a limited number of values
    bin counting = count ocorrenze (from Age to Is_maggiorenne)

    NOTE: introducing scaling is altering the interpretation of the model


FEATURE SELECTION
The Decision Tree classifier (and variants of) attempts to segment the data into "pure" buckets via simple thresholds. The split that produces the most "pure" bucket is deemed to be a good split.

We can use this definition of "good" to provide some information about how well a single feature is able to split segment the data according to the labels. "Better" features will have a higher score.

        import pandas as pd
        import sklearn.tree
        import numpy as np
        import matplotlib.pyplot as plt

        url = "../data/titanic.csv"
        titanic = pd.read_csv(url)
        X = titanic[['age', 'fare', 'survived']]
        X = X.dropna()
        y = X[['survived']]
        X = X[['age', 'fare']]

        mdl = sklearn.tree.DecisionTreeClassifier().fit(X, y)

        plt.bar(X.columns, mdl.feature_importances_)
        plt.gca().set_ylabel('Relative importance');

Brute Force
Another thing we can do is iterate over features to find the best combination. Let's use mlxtend to implement this for us (and borrow an example)

        !pip install mlxtend > /dev/null

        from mlxtend.feature_selection import SequentialFeatureSelector as SFS
        from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
        from sklearn.neighbors import KNeighborsClassifier
        from mlxtend.data import wine_data
        from sklearn.model_selection import train_test_split
        from sklearn.preprocessing import StandardScaler
        from sklearn.pipeline import make_pipeline

        X, y = wine_data()
        X_train, X_test, y_train, y_test= train_test_split(X, y, 
                                                           stratify=y,
                                                           test_size=0.3,
                                                           random_state=1)

        knn = KNeighborsClassifier(n_neighbors=2)

        sfs1 = SFS(estimator=knn, 
                   k_features=(3, 10),
                   forward=True, 
                   floating=False, 
                   scoring='accuracy',
                   cv=5)

        pipe = make_pipeline(StandardScaler(), sfs1)

        pipe.fit(X_train, y_train)

        print('best combination (ACC: %.3f): %s\n' % (sfs1.k_score_, sfs1.k_feature_idx_))
        # print('all subsets:\n', sfs1.subsets_)
        plot_sfs(sfs1.get_metric_dict(), kind='std_err');


Iris dataset:

        from sklearn.datasets import load_iris
        from mlxtend.evaluate import PredefinedHoldoutSplit
        import numpy as np


        iris = load_iris()
        X = iris.data
        y = iris.target

Above is the infamous iris dataset.
    Can you tell me which feature is the most informative?

        nn = KNeighborsClassifier(n_neighbors=2)

        sfs1 = SFS(estimator=knn, 
                   k_features=(1, 3),
                   forward=True, 
                   floating=False, 
                   scoring='accuracy',
                   cv=5)

        pipe = make_pipeline(StandardScaler(), sfs1)

        pipe.fit(X, y)

        print('best combination (ACC: %.3f): %s\n' % (sfs1.k_score_, sfs1.k_feature_idx_))
        # print('all subsets:\n', sfs1.subsets_)
        plot_sfs(sfs1.get_metric_dict(), kind='std_err');

TIMESERIES DATA

Your domain might involve timeseries data. Much of what we have discussed also applies to the time domain. However there is a significant amount of knowledge that has been accumulated under the specialisation of Signal Processing.

But just to give you an idea, lets use the library statsmodels to give you some idea.

        url = "../data/airline-passengers.csv"
        airline = pd.read_csv(url)
        airline.columns = ['month', 'passengers']
        airline['month'] = pd.to_datetime(airline['month'])
        airline.set_index('month', inplace=True)
        airline.index = pd.DatetimeIndex(airline.index.values,freq=airline.index.inferred_freq)
        airline.plot();

First let me show you how to do some exponential smoothing. This is a simple averaging technique to perform "noise reduction". Note that we're not really reducing any noise, we're just saying that the rolling average of the data is a better representation of that data.

        from statsmodels.tsa.api import SimpleExpSmoothing

        airline.plot()

        # Simple Exponential Smoothing
        for alpha in [0.3, 0.1]:
            fit = SimpleExpSmoothing(airline).fit(smoothing_level=alpha,optimized=False)
            fit.fittedvalues.rename(r'$\alpha={}$'.format(alpha)).plot()
        plt.legend();

        airline_corrupt = airline.copy()
        airline_corrupt.iloc[10:11] = np.nan
        airline_corrupt.iloc[30:35] = np.nan
        airline_corrupt.iloc[60:100] = np.nan

for linear interpolation fo the gap:

        airline_corrupt.interpolate().plot();

con reti neurali si rimuovono campioni per verificare quali filtri funzionano meglio sui dati

https://0636920248866.oreilly-jupyterhub.com/user/c95b6c58-548d-4a4f-bbba-b3373ccedc03/notebooks/solutions/notebook.ipynb
https://0636920248866.oreilly-jupyterhub.com/user/c95b6c58-548d-4a4f-bbba-b3373ccedc03/notebooks/notebooks/notebook.ipynb
https://resources.oreilly.com/live-training/cleaning-data-at-scale


