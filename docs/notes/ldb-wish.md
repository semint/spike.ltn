## WISH:

$ wd ltn
$ source ./envltn/bin/activate
$ cd lsh
$ python3

>>> import ldb
>>> print(ldb.constants)
>>> print(ldb.variables)
>>> print(ldb.predicates
>>> print(ldb.atoms)
>>> pd.DataFrame(ldb.atoms,index=[0])
>>> pd.DataFrame(ldb.atoms,index=[0]).values.tolist()


>>> ldb.clear_db()
>>> ldb.read_db()
>>> ldb.print_db()
>>> ldb.train()
>>> ldb.ask_atoms()
>>> ldb.ask('')



## TODO:


* mondo chiuso, atomi negati


## FIRST SESSION SAMPLE:

        $ python3
        Python 3.5.2 (default, Nov 12 2018, 13:43:14) 
        [GCC 5.4.0 20160609] on linux
        Type "help", "copyright", "credits" or "license" for more information.
        >>> import lsh
        ATOMS:
           0  1
        0  L  a
        1  L  b
        2  M  a
        3  Z  c
        4  Y  d
        PREDICATES
        L
        M
        Z
        Y
        VARIABLES:
        x
        y
        z
        CONSTANTS
        a
        b
        c
        d
        FACTS:
        forall x:(S(x) -> M(x))

        forall x:(L(x) -> M(x))

        forall x:(S(x) % exists y:P(x,y))

        forall x:~P(x,x)

        forall x,y:(P(x,y) -> P(y,x))

        forall x,y,z:(P(x,y) & P(x,z) -> eq(y,z))

        >>> lsh.ltn_train()
        EMBEDDING_SIZE:2
        learning_rate:0.1
        max_epochs:4000
        WARNING:tensorflow:From /home/nipe/.local/lib/python3.5/site-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.
        Instructions for updating:
        Colocations handled automatically by placer.
        2019-03-08 16:06:33.632255: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
        2019-03-08 16:06:33.752406: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2711950000 Hz
        2019-03-08 16:06:33.757106: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x3c41210 executing computations on platform Host. Devices:
        2019-03-08 16:06:33.757159: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): <undefined>, <undefined>
        WARNING:tensorflow:From /home/nipe/.local/lib/python3.5/site-packages/tensorflow/python/ops/math_ops.py:3066: to_int32 (from tensorflow.python.ops.math_ops) is deprecated and will be removed in a future version.
        Instructions for updating:
        Use tf.cast instead.
        >>> lsh.ask_atoms()
        L(a) [0.9999826]
        M(a) [0.9997838]
        Z(a) [0.9779037]
        Y(a) [0.9998242]
        L(b) [0.99974126]
        M(b) [0.9982326]
        Z(b) [0.9601056]
        Y(b) [0.99866223]
        L(c) [0.9899553]
        M(c) [0.9541643]
        Z(c) [0.9997589]
        Y(c) [0.99401337]
        L(d) [0.99995685]
        M(d) [0.9995067]
        Z(d) [0.99452764]
        Y(d) [0.999782]


## SECOND SESSION SAMPLE

        >>> lsh.clear_db()
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        AttributeError: module 'lsh' has no attribute 'clear_db'
        >>> lsh.clean_db()
        >>> lsh.read_db()
        >>> lsh.print_sb()
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        AttributeError: module 'lsh' has no attribute 'print_sb'
        >>> lsh.print_db()
        ATOMS:
            0  1
        0  ~L  a
        1   L  b
        2   M  a
        3   Z  c
        4   Y  d
        PREDICATES
        ~L
        L
        M
        Z
        Y
        VARIABLES:
        x
        y
        z
        CONSTANTS
        a
        b
        c
        d
        FACTS:
        forall x:(S(x) -> M(x))

        forall x:(L(x) -> M(x))

        forall x:(S(x) % exists y:P(x,y))

        forall x:~P(x,x)

        forall x,y:(P(x,y) -> P(y,x))

        forall x,y,z:(P(x,y) & P(x,z) -> eq(y,z))

        >>> lsh.ltn_train()
        EMBEDDING_SIZE:2
        learning_rate:0.1
        max_epochs:4000
        Redeclaring existing constant a
        Redeclaring existing constant b
        Redeclaring existing constant c
        Redeclaring existing constant d
        Redeclaring existing predicate eq
        Redeclaring existing predicate L
        Redeclaring existing predicate M
        Redeclaring existing predicate Z
        Redeclaring existing predicate Y
        Redeclaring existing variable x
        Redeclaring existing variable y
        Redeclaring existing variable z
        Closing existing Tensorflow session.
        >>> lsh.ask_atoms()
        ~L(a) [0.9997275]
        L(a) [0.00027249]
        M(a) [0.51770896]
        Z(a) [0.6316161]
        Y(a) [0.6337249]
        ~L(b) [0.56604487]
        L(b) [0.43395513]
        M(b) [0.5006146]
        Z(b) [0.50114036]
        Y(b) [0.50675374]
        ~L(c) [0.9248146]
        L(c) [0.07518542]
        M(c) [0.5053691]
        Z(c) [0.5328958]
        Y(c) [0.54625547]
        ~L(d) [0.7166512]
        L(d) [0.28334883]
        M(d) [0.5026567]
        Z(d) [0.49319142]
        Y(d) [0.5348544]
        >>> exit()

# assiomatizzazione db
indipendente dal numero di righe
dipendente dal numero di valori distinti presente per ciascuna colonna

con ogni colonna crea una enum con i valori distinti presenti

crea un range per ogni variabile sugli identificatori di tupla che assumono quel valore:

x11[t1, t2]
x12[t3]

x21[t1,t3]
x22[t2]

per ogni valore nella enum crea una formula di definizione per esclusione

es a 4 valori:
for all x11:   v11 & -v12 & -v13 & -v14
for all x12:  -v11 &  v12 & -v13 & -v14
for all x13:  -v11 & -v12 &  v13 & -v14
for all x14:  -v11 & -v12 & -v13 &  v14

es. a 2 valori:
for all x21:  v21 & -v22
for all x22: -v21 &  v22


