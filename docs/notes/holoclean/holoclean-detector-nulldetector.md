# HoloClean - detector - nulldetector

https://github.com/HoloClean/holoclean/blob/master/detect/nulldetector.py

detect_noisy_cells returns a pandas.DataFrame containing all cells with NULL values.
:return: pandas.DataFrame with columns:
            _tid_: entity ID
            attribute: attribute with NULL value for this entity

env not used
dataset get_raw_data()
