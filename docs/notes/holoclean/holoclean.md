# HOLOCLEAN

A variety of signals can be useful for data cleaning: 
integrity constraints, 
external information in the form of dictionaries,and 
basic quantitative statistics of the dataset to be cleaned.

Holoclean uses all of these.

A probabilistic inference program involves two tasks:  

(i) grounding, which enumerates all possible interactions between correlated random variables to materialize a factor graph that represents the joint distribution  over  all  variables,  and  

(ii) inference, where  the  goal  is  to compute the marginal probability for every random variable. 

Each repair proposed by HoloClean is associated with a marginal probability.
We can ask users to verify repairs with low marginal probabilities and use those as labeled examples to retrainthe parameters.


 Holo-Clean is built on top of DeepDive, a declarative probabilisticinference engine.