# HoloClean - detector - errorloaderdetector

        To load from csv file, :param fpath: must be specified.
        To load from a relational table, :param db_engine:, and 
        :param table_name: must be specified, optionally specifying :param schema_name:.
 
Returns a pandas.DataFrame containing loaded errors from a source.
        :return: pandas.DataFrame with columns:
            id_col: entity ID
            attr_col: attribute in violation
