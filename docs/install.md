# Install

This is an install-log and how to solve common problems on a ubuntu machine of LTN with a smoking-test run example.

## know ubuntu version

    ~/Spikespaces/python/ltn/logictensornetworks
    $ uname -a
    Linux tao 4.4.0-141-generic #167-Ubuntu SMP Wed Dec 5 10:40:15 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
    (ltn) 

## basic install

    pip install tensorflow numpy matplotlib

## problem 1: env

    Could not install packages due to an EnvironmentError: [Errno 13] Permission denied: '/usr/local/lib/python3.5/dist-packages/grpc'
    Consider using the `--user` option or check the permissions.

### solution 1: use venv

    python3 -m venv ltn
    source ./ltn/bin/activate 
    python -m pip pip install tensorflow numpy matplotlib

## problem 2: modules

    cd ~/Spikespaces/python/ltn/logictensornetworks/examples_ltn

    $ python smokes_friends_cancer.py 
    Traceback (most recent call last):
      File "smokes_friends_cancer.py", line 3, in <module>
        import logictensornetworks as ltn
    ImportError: No module named 'logictensornetworks'
    (ltn) 

### solution 2: create a module folder

    ~/Spikespaces/python/ltn/logictensornetworks on  master ⌚ 9:13:11
    $ mkdir ~/Spikespaces/python/extramodules

    ~/Spikespaces/python/ltn/logictensornetworks on  master ⌚ 9:14:17
    $ cp *.py ~/Spikespaces/python/extramodules 

    ~/Spikespaces/python/ltn/logictensornetworks on  master ⌚ 9:14:36
    $ ls ~/Spikespaces/python/extramodules
    __init__.py  logictensornetworks_library.py  logictensornetworks.py  logictensornetworks_wrapper.py

    $ echo $PYTHONPATH
    /qgispath/share/qgis/python:/home/nipe/Spikespaces/python/extramodules


## problem 3: tkinter

    $ python examples_ltn/smokes_friends_cancer.py 
    Traceback (most recent call last):
      File "/usr/lib/python3.5/tkinter/__init__.py", line 36, in <module>
        import _tkinter
    ImportError: No module named '_tkinter'

    During handling of the above exception, another exception occurred:

    Traceback (most recent call last):
      File "examples_ltn/smokes_friends_cancer.py", line 4, in <module>
        import matplotlib.pyplot as plt
      File "/home/nipe/Spikespaces/python/ltn/lib/python3.5/site-packages/matplotlib/pyplot.py", line 2374, in <module>
        switch_backend(rcParams["backend"])
      File "/home/nipe/Spikespaces/python/ltn/lib/python3.5/site-packages/matplotlib/pyplot.py", line 207, in switch_backend
        backend_mod = importlib.import_module(backend_name)
      File "/usr/lib/python3.5/importlib/__init__.py", line 126, in import_module
        return _bootstrap._gcd_import(name[level:], package, level)
      File "/home/nipe/Spikespaces/python/ltn/lib/python3.5/site-packages/matplotlib/backends/backend_tkagg.py", line 1, in <module>
        from . import _backend_tk
      File "/home/nipe/Spikespaces/python/ltn/lib/python3.5/site-packages/matplotlib/backends/_backend_tk.py", line 5, in <module>
        import tkinter as Tk
      File "/usr/lib/python3.5/tkinter/__init__.py", line 38, in <module>
        raise ImportError(str(msg) + ', please install the python3-tk package')
    ImportError: No module named '_tkinter', please install the python3-tk package


### soution 3: install python3-tk

    ~/Spikespaces/python/ltn/logictensornetworks
    $ sudo apt install python3-tk
    Reading package lists... Done
    Building dependency tree       
    Reading state information... Done
    The following packages were automatically installed and are no longer required:
      libllvm4.0 libllvm4.0:i386 libllvm5.0 libllvm5.0:i386 libqmi-glib1 libqpdf17 linux-headers-4.4.0-128 linux-headers-4.4.0-128-generic linux-headers-4.4.0-130 linux-headers-4.4.0-130-generic linux-headers-4.4.0-131
      linux-headers-4.4.0-131-generic linux-headers-4.4.0-133 linux-headers-4.4.0-133-generic linux-headers-4.4.0-134 linux-headers-4.4.0-134-generic linux-headers-4.4.0-137 linux-headers-4.4.0-137-generic linux-headers-4.4.0-138
      linux-headers-4.4.0-138-generic linux-headers-4.4.0-139 linux-headers-4.4.0-139-generic linux-image-4.4.0-128-generic linux-image-4.4.0-130-generic linux-image-4.4.0-131-generic linux-image-4.4.0-133-generic
      linux-image-4.4.0-134-generic linux-image-4.4.0-137-generic linux-image-4.4.0-138-generic linux-image-4.4.0-139-generic linux-image-extra-4.4.0-128-generic linux-image-extra-4.4.0-130-generic linux-image-extra-4.4.0-131-generic
      linux-image-extra-4.4.0-133-generic linux-image-extra-4.4.0-134-generic linux-image-extra-4.4.0-137-generic linux-image-extra-4.4.0-138-generic linux-image-extra-4.4.0-139-generic linux-signed-image-4.4.0-128-generic
      linux-signed-image-4.4.0-130-generic linux-signed-image-4.4.0-131-generic linux-signed-image-4.4.0-133-generic linux-signed-image-4.4.0-134-generic linux-signed-image-4.4.0-137-generic linux-signed-image-4.4.0-138-generic
      linux-signed-image-4.4.0-139-generic
    Use 'sudo apt autoremove' to remove them.
    Suggested packages:
      tix python3-tk-dbg
    The following NEW packages will be installed:
      python3-tk
    0 upgraded, 1 newly installed, 0 to remove and 14 not upgraded.
    Need to get 25,1 kB of archives.
    After this operation, 89,1 kB of additional disk space will be used.
    Get:1 http://it.archive.ubuntu.com/ubuntu xenial/main amd64 python3-tk amd64 3.5.1-1 [25,1 kB]
    Fetched 25,1 kB in 0s (84,7 kB/s)   
    Selecting previously unselected package python3-tk.
    (Reading database ... 614011 files and directories currently installed.)
    Preparing to unpack .../python3-tk_3.5.1-1_amd64.deb ...
    Unpacking python3-tk (3.5.1-1) ...
    Setting up python3-tk (3.5.1-1) ...


## problem 4: pandas

    $ python examples_ltn/smokes_friends_cancer.py

    Traceback (most recent call last):
      File "examples_ltn/smokes_friends_cancer.py", line 6, in <module>
        import pandas as pd
    ImportError: No module named 'pandas'

### solution 4: install pandas

    $ pip install pandas

    Collecting pandas
      Downloading https://files.pythonhosted.org/packages/5d/d4/6e9c56a561f1d27407bf29318ca43f36ccaa289271b805a30034eb3a8ec4/pandas-0.23.4-cp35-cp35m-manylinux1_x86_64.whl (8.7MB)
        100% |████████████████████████████████| 8.7MB 912kB/s 
    Collecting pytz>=2011k (from pandas)
      Downloading https://files.pythonhosted.org/packages/61/28/1d3920e4d1d50b19bc5d24398a7cd85cc7b9a75a490570d5a30c57622d34/pytz-2018.9-py2.py3-none-any.whl (510kB)
        100% |████████████████████████████████| 512kB 1.3MB/s 
    Requirement already satisfied: numpy>=1.9.0 in ./lib/python3.5/site-packages (from pandas) (1.16.0)
    Requirement already satisfied: python-dateutil>=2.5.0 in ./lib/python3.5/site-packages (from pandas) (2.7.5)
    Requirement already satisfied: six>=1.5 in ./lib/python3.5/site-packages (from python-dateutil>=2.5.0->pandas) (1.12.0)
    Installing collected packages: pytz, pandas
    Successfully installed pandas-0.23.4 pytz-2018.9
    (ltn) 

## run

    ~/Spikespaces/python/ltn/logictensornetworks

    $ python examples_ltn/smokes_friends_cancer.py
    2019-01-18 09:26:16.175070: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
    2019-01-18 09:26:20.437596: E tensorflow/core/grappler/optimizers/dependency_optimizer.cc:666] Iteration = 0, topological sort failed with message: The graph couldn't be sorted in topological order.
    2019-01-18 09:26:21.648892: E tensorflow/core/grappler/optimizers/dependency_optimizer.cc:666] Iteration = 1, topological sort failed with message: The graph couldn't be sorted in topological order.
    2019-01-18 09:26:32.114611: E tensorflow/core/grappler/optimizers/dependency_optimizer.cc:666] Iteration = 0, topological sort failed with message: The graph couldn't be sorted in topological order.
    2019-01-18 09:26:32.282299: E tensorflow/core/grappler/optimizers/dependency_optimizer.cc:666] Iteration = 1, topological sort failed with message: The graph couldn't be sorted in topological order.
    0 =====> 0.14366668 -4.9189075e-09
    100 =====> 0.74164474 3.0599163e-09
    200 =====> 0.8866052 7.92616e-09
    300 =====> 0.92904514 8.264965e-09
    400 =====> 0.95162517 8.320499e-09
    500 =====> 0.9886113 8.330669e-09
    600 =====> 0.99457943 8.332502e-09
    700 =====> 0.99655426 8.332918e-09
    800 =====> 0.9973558 8.333065e-09
    900 =====> 0.99779224 8.333136e-09
    1000 =====> 0.9979419 8.333178e-09
    1100 =====> 0.9980184 8.333206e-09
    1200 =====> 0.9980432 8.333224e-09
    1300 =====> 0.9980427 8.333237e-09
    1400 =====> 0.9980647 8.33325e-09
    1500 =====> 0.9980743 8.3332585e-09
    1600 =====> 0.99806565 8.333266e-09
    1700 =====> 0.9980605 8.333272e-09
    1800 =====> 0.99806947 8.333276e-09
    1900 =====> 0.9980793 8.333279e-09
    2000 =====> 0.9980862 8.333283e-09
    2100 =====> 0.99808526 8.333288e-09
    2200 =====> 0.99807894 8.33329e-09
    2300 =====> 0.99807465 8.333291e-09
    2400 =====> 0.998072 8.333296e-09
    2500 =====> 0.9980704 8.333298e-09
    2600 =====> 0.99807274 8.333298e-09
    2700 =====> 0.9980799 8.3333e-09
    2800 =====> 0.998085 8.333302e-09
    2900 =====> 0.9980888 8.333303e-09
    3000 =====> 0.9980914 8.333303e-09
    3100 =====> 0.9980933 8.3333065e-09
    3200 =====> 0.99809474 8.3333065e-09
    3300 =====> 0.9980957 8.333307e-09
    3400 =====> 0.9980965 8.333307e-09
    3500 =====> 0.998097 8.333309e-09
    3600 =====> 0.9980971 8.333309e-09
    3700 =====> 0.9980975 8.333312e-09
    3800 =====> 0.9980975 8.333312e-09
    3900 =====> 0.9980976 8.333312e-09
    4000 =====> 0.9980975 8.333314e-09
    4100 =====> 0.9980975 8.333314e-09
    4200 =====> 0.9980975 8.333314e-09
    4300 =====> 0.99809736 8.333314e-09
    4400 =====> 0.9980975 8.333315e-09
    4500 =====> 0.99809736 8.333315e-09
    4600 =====> 0.99809724 8.333315e-09
    4700 =====> 0.9980971 8.333315e-09
    4800 =====> 0.9980971 8.333315e-09
    4900 =====> 0.998097 8.333318e-09
    5000 =====> 0.998097 8.333318e-09
    5100 =====> 0.998097 8.333318e-09
    5200 =====> 0.998097 8.333318e-09
    5300 =====> 0.9980969 8.333318e-09
    5400 =====> 0.9980969 8.33332e-09
    5500 =====> 0.99809676 8.33332e-09
    5600 =====> 0.99809676 8.33332e-09
    5700 =====> 0.99809676 8.33332e-09
    5800 =====> 0.99809664 8.33332e-09
    5900 =====> 0.99809664 8.33332e-09
    6000 =====> 0.99809676 8.33332e-09
    6100 =====> 0.99809664 8.33332e-09
    6200 =====> 0.9980965 8.333321e-09
    6300 =====> 0.9980965 8.333321e-09
    6400 =====> 0.99809664 8.333321e-09
    6500 =====> 0.9980965 8.333321e-09
    6600 =====> 0.9980965 8.333321e-09
    6700 =====> 0.9980964 8.333321e-09
    6800 =====> 0.9980963 8.333321e-09
    6900 =====> 0.9980964 8.333321e-09
    7000 =====> 0.9980965 8.333321e-09
    7100 =====> 0.9980964 8.333321e-09
    7200 =====> 0.9980964 8.333324e-09
    7300 =====> 0.9980964 8.333324e-09
    7400 =====> 0.9980964 8.333324e-09
    7500 =====> 0.9980963 8.333324e-09
    7600 =====> 0.9980964 8.333324e-09
    7700 =====> 0.9980964 8.333324e-09
    7800 =====> 0.9980963 8.333324e-09
    7900 =====> 0.9980963 8.333324e-09
    8000 =====> 0.9980963 8.333324e-09
    8100 =====> 0.99809617 8.333324e-09
    8200 =====> 0.9980963 8.333324e-09
    8300 =====> 0.9980963 8.333324e-09
    8400 =====> 0.9980963 8.333324e-09
    8500 =====> 0.9980963 8.333324e-09
    8600 =====> 0.9980963 8.333324e-09
    8700 =====> 0.9980963 8.333325e-09
    8800 =====> 0.99809617 8.333325e-09
    8900 =====> 0.99809617 8.333325e-09
    9000 =====> 0.99809617 8.333325e-09
    9100 =====> 0.99809617 8.333325e-09
    9200 =====> 0.9980963 8.333325e-09
    9300 =====> 0.99809617 8.333325e-09
    9400 =====> 0.9980963 8.333325e-09
    9500 =====> 0.9980963 8.333325e-09
    9600 =====> 0.9980963 8.333325e-09
    9700 =====> 0.9980963 8.333325e-09
    9800 =====> 0.99809617 8.333325e-09
    9900 =====> 0.9980964 8.333325e-09
    forall x ~Friends(x,x) [0.9285713]
    Forall x Smokes(x) -> Cancer(x) [0.7495755]
    forall x y Friends(x,y) -> Friends(y,x) [0.9744898]
    forall x Exists y (Friends(x,y) [0.8571417]
    Forall x,y Friends(x,y) -> (Smokes(x)->Smokes(y)) [0.96938777]
    Forall x: smokes(x) -> forall y: friend(x,y) -> smokes(y)) [0.9693878]

A window with diagrams should now be open.
