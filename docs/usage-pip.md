# LTNW Database sample usage with PIP:

Install required python libraries [Install](install.md)

	$ cd docker/docker-ldb/ldb
	$ python3
	>>> import sample.basic_category 

example output:

	Python 3.6.8 (default, Jan 14 2019, 11:02:34) 
	[GCC 8.0.1 20180414 (experimental) [trunk revision 259383]] on linux
	Type "help", "copyright", "credits" or "license" for more information.
	>>> import sample.basic_category
	['a', 'b', 'c', 'd', 'e', 'f']
	WARNING:tensorflow:From /home/nicola/.local/lib/python3.6/site-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.
	Instructions for updating:
	Colocations handled automatically by placer.
	INFO:code.logictensornetworks_wrapper:Initializing knowledgebase
	INFO:code.logictensornetworks_wrapper:Initializing Tensorflow session
	2019-11-28 11:10:09.370573: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
	2019-11-28 11:10:09.408097: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2400000000 Hz
	2019-11-28 11:10:09.409336: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x234f9c0 executing computations on platform Host. Devices:
	2019-11-28 11:10:09.409371: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): <undefined>, <undefined>
	INFO:code.logictensornetworks_wrapper:Initializing optimizer
	WARNING:tensorflow:From /home/nicola/.local/lib/python3.6/site-packages/tensorflow/python/ops/math_ops.py:3066: to_int32 (from tensorflow.python.ops.math_ops) is deprecated and will be removed in a future version.
	Instructions for updating:
	Use tf.cast instead.
	WARNING:tensorflow:From /home/nicola/.local/lib/python3.6/site-packages/tensorflow/python/ops/math_grad.py:102: div (from tensorflow.python.ops.math_ops) is deprecated and will be removed in a future version.
	Instructions for updating:
	Deprecated in favor of operator or tf.math.divide.
	INFO:code.logictensornetworks_wrapper:Assembling feed dict
	INFO:code.logictensornetworks_wrapper:INITIALIZED with sat level = 0.77865845
	INFO:code.logictensornetworks_wrapper:TRAINING 0 sat level -----> 0.77865845
	INFO:code.logictensornetworks_wrapper:TRAINING 100 sat level -----> 0.84421855
	INFO:code.logictensornetworks_wrapper:TRAINING 200 sat level -----> 0.9149913
	INFO:code.logictensornetworks_wrapper:TRAINING 300 sat level -----> 0.9761693
	INFO:code.logictensornetworks_wrapper:TRAINING 400 sat level -----> 0.9964056
	INFO:code.logictensornetworks_wrapper:TRAINING finished after 400 epochs with sat level 0.9964056
	Married(a) [0.35739693]
	Worker(a) [0.9892168]
	Adult(a) [0.98922133]
	~Adult(a) [0.01077867]
	Worker(b) [0.6251216]
	Adult(b) [0.61235267]
	Worker(c) [0.56209445]
	Adult(c) [0.5747912]

