## Database Repair with Logic Tensor Networks

This is a basic example in which we use LTN for unsupervised in bringing out
database population problems. We define a theory that encodes the following
facts
* every person in the sample set should be assigned to a profile
* every
person may be 
* if two points are close, they should belong to the same cluster
We start setting loggin and importing required modules.

```{.python .input  n=1}
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np
import tensorflow as tf
import logictensornetworks_wrapper as ltnw
```

One notably successful use of deep learning is embedding, a method used to
represent discrete variables as continuous vectors.

Per each person, six
people, we define a constant with embedding.

```{.python .input  n=2}
EMBEDDING_SIZE = 2
constants = list('abcdef')
print(constants)

for c in constants:
    ltnw.constant(c,min_value=[0.]*EMBEDDING_SIZE,max_value=[1.]*EMBEDDING_SIZE)
```

We define a simple net as a sigmoid function of belonging.

```{.python .input}
def simple_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE), stddev=0.1))
    def net(x):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(x,N),x),axis=1))
    return net

```

We define a double net as a sigmoid over a variable with double embedding.

```{.python .input  n=4}
def double_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE*2, EMBEDDING_SIZE*2), stddev=0.1))
    def net(x,y):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(tf.concat([x,y],axis=1),N),tf.concat([x,y],axis=1)),axis=1))
    return net

```

We define the theory using a number of predicates and variables.
* M for male
people.
* S for married people.
* L for workers.
* P for our query predicate.

```{.python .input  n=5}
ltnw.predicate("M",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("S",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("L",EMBEDDING_SIZE,pred_definition=simple_net())

ltnw.predicate("P",EMBEDDING_SIZE*2,pred_definition=double_net())

```

We define our population.

```{.python .input  n=6}
ltnw.axiom('M(a)')
ltnw.axiom('~L(a)')
ltnw.axiom('S(a)')

ltnw.axiom('M(b)')
ltnw.axiom('L(b)')
ltnw.axiom('~S(b)')

ltnw.axiom('M(c)')
ltnw.axiom('L(c)')
ltnw.axiom('S(c)')

ltnw.axiom('M(d)')
ltnw.axiom('L(d)')
ltnw.axiom('~S(d)')

ltnw.axiom('~M(e)')
ltnw.axiom('~L(e)')
ltnw.axiom('~S(e)')

ltnw.axiom('~M(f)')
ltnw.axiom('~L(f)')
ltnw.axiom('S(f)')

```

Next we can define the theory using three variables.

```{.python .input}
ltnw.variable('x',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))
ltnw.variable('y',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))
ltnw.variable('z',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))

```

We define the equality for two individuals.

```{.python .input}
def smooth_eq(x,y):
    return 1 - tf.truediv(tf.reduce_sum(tf.square(x - y), axis=1),
                          np.float32(EMBEDDING_SIZE)*tf.square(8.))

ltnw.predicate('eq',EMBEDDING_SIZE*2,
               pred_definition=smooth_eq)

```

The relationships:
* married requires male.
* worker required male.

```{.python .input}
ltnw.axiom('forall x:(S(x) -> M(x))')
ltnw.axiom('forall x:(L(x) -> M(x))')
```

We define our query predicate:
* P is married.
* P is not married with his self.
* P is reflexive.
* P is intransitive.

```{.python .input}
ltnw.axiom('forall x:(S(x) % exists y:P(x,y))')
ltnw.axiom('forall x:~P(x,x)')
ltnw.axiom('forall x,y:(P(x,y) -> P(y,x))')
ltnw.axiom('forall x,y,z:(P(x,y) & P(x,z) -> eq(y,z))')

```

We train our knowledgbase model:

```{.python .input}
ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(learning_rate=.01),
                              initial_sat_level_threshold=.1)
sat_level=ltnw.train(track_sat_levels=100,sat_level_epsilon=.99,max_epochs=4000)

```

We ask our model if there are people not satisfing constrains:

```{.python .input}
print('L(x) -> M(x)',constants[np.where(ltnw.ask('L(x) -> M(x)') < .95)[0].squeeze()])
print('S(x) -> M(x)',constants[np.where(ltnw.ask('S(x) -> M(x)') < .95)[0].squeeze()])

```

    L(x) -> M(x) []
    S(x) -> M(x) f

We ask our model what the computed populations looks like:

```{.python .input}
for c in constants:
    for p in ['L','S','M']:
        print("{}({})".format(p,c),ltnw.ask("{}({})".format(p,c)))
    print('L({}) -> M({})'.format(c,c),ltnw.ask('L({}) -> M({})'.format(c,c)))
    print('S({}) -> M({})'.format(c,c),ltnw.ask('S({}) -> M({})'.format(c,c)))

```

    L(a) [1.0207855e-06]
    S(a) [1.]
    M(a) [0.9999863]
    L(a) -> M(a)
[1.]
    S(a) -> M(a) [0.9999863]
    L(b) [1.]
    S(b) [6.727357e-13]
    M(b)
[1.]
    L(b) -> M(b) [1.]
    S(b) -> M(b) [1.]
    L(c) [0.99999976]
    S(c)
[1.]
    M(c) [1.]
    L(c) -> M(c) [1.]
    S(c) -> M(c) [1.]
    L(d) [1.]
S(d) [6.5612733e-13]
    M(d) [1.]
    L(d) -> M(d) [1.]
    S(d) -> M(d) [1.]
L(e) [5.8702336e-36]
    S(e) [2.572155e-07]
    M(e) [1.069422e-06]
    L(e) ->
M(e) [1.]
    S(e) -> M(e) [1.]
    L(f) [2.6762434e-06]
    S(f) [0.9996828]
M(f) [0.29834983]
    L(f) -> M(f) [1.]
    S(f) -> M(f) [0.29866704]

We print the query result:

```{.python .input}
for c in constants:
    for d in constants:
        query = 'P({},{})'.format(c,d)
        print(query,ltnw.ask(query))

```

    P(a,a) [3.3316623e-09]
    P(a,b) [4.991514e-07]
    P(a,c) [1.]
    P(a,d)
[4.976679e-07]
    P(a,e) [4.8582515e-27]
    P(a,f) [1.4463134e-06]
    P(b,a)
[7.499673e-09]
    P(b,b) [1.1091982e-23]
    P(b,c) [3.3371647e-12]
    P(b,d)
[1.0879146e-23]
    P(b,e) [5.376541e-37]
    P(b,f) [1.5931385e-11]
    P(c,a)
[1.]
    P(c,b) [1.9576075e-10]
    P(c,c) [8.435345e-12]
    P(c,d)
[1.9153001e-10]
    P(c,e) [2.1115423e-09]
    P(c,f) [0.9996495]
    P(d,a)
[7.451238e-09]
    P(d,b) [1.0841032e-23]
    P(d,c) [3.252593e-12]
    P(d,d)
[1.0632929e-23]
    P(d,e) [5.286567e-37]
    P(d,f) [1.5748153e-11]
    P(e,a)
[2.3634969e-29]
    P(e,b) [7.868999e-38]
    P(e,c) [6.0344286e-12]
    P(e,d)
[7.7617916e-38]
    P(e,e) [0.]
    P(e,f) [6.672328e-29]
    P(f,a)
[9.0643476e-07]
    P(f,b) [7.0846756e-10]
    P(f,c) [0.9997254]
    P(f,d)
[7.0280004e-10]
    P(f,e) [1.145035e-26]
    P(f,f) [7.1356444e-06]

We can see there is a possible world where:
* f is female.
* a is married with c
* c is married with f
