
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np

import tensorflow as tf

import logictensornetworks_wrapper as ltnw

ltnw.constant('a',[1.,2.,3.,15])
ltnw.constant('b',[1.,2.,3.,16])

ltnw.constant('roma',[12.,41.])
ltnw.constant('parigi',[48.,2.])
ltnw.constant('frascati',[13.,42.])


ltnw.function("eta", 4, fun_definition = lambda x:x[:,3])
ltnw.initialize_knowledgebase()
def _crisp_leq(x,y):
    return tf.cast(tf.less_equal(x,y),dtype=tf.float32)
def _dist(x,y):
    return tf.exp(-tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(x,y)),axis=1,keepdims=True)))
ltnw.predicate("cleq",2,_crisp_leq)
ltnw.predicate("close",4,_dist)
print(ltnw.ask("cleq(eta(b),eta(a))"))
print(ltnw.ask('close(roma,parigi)'))
print(ltnw.ask('close(roma,roma)'))





