# -*- coding: utf-8 -*-
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import logictensornetworks as ltn
import logictensornetworks_wrapper as ltnw

#  Hyper-parameters
ltn.LAYERS = 10
ltnw.set_universal_aggreg('min')
EMBEDDING_SIZE = 6

# KNOWLEDGE

constants = list('abcdef')
print(constants)
for c in constants:
    ltnw.constant(c,
                  min_value=[0.]*EMBEDDING_SIZE, 
                  max_value=[1.]*EMBEDDING_SIZE)


def simple_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE),
                    stddev=0.1))

    def net(x):
        a = tf.sigmoid(tf.reduce_sum(
                          tf.multiply(tf.matmul(x, N), x), axis=1))
        print("A SHAPE: {}".format(a.shape))
        return a
    return net

ltnw.predicate("Adult", EMBEDDING_SIZE, pred_definition=simple_net())
ltnw.predicate("Married", EMBEDDING_SIZE, pred_definition=simple_net())
ltnw.predicate("Worker", EMBEDDING_SIZE, pred_definition=simple_net())

ltnw.variable('x', tf.concat(
    [ltnw.CONSTANTS[c] for c in constants if c not in ['b']], axis=0))
#    [ltnw.CONSTANTS[c] for c in constants], axis=0))


# RULES

# If Married then is Adult.
ltnw.axiom('forall x:(Married(x) -> Adult(x))')

# If Worker then is Adult.
ltnw.axiom('forall x:(Worker(x) -> Adult(x))')


# Constraints

ltnw.axiom('~Adult(a)')
#ltnw.axiom('Worker(a)')
#ltnw.axiom('~Married(a)')
#ltnw.axiom('Worker(b)')


# *** TRAIN

ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(
                              learning_rate=.001),
                              initial_sat_level_threshold=.1)

sat_level = ltnw.train(track_sat_levels=100,
                       sat_level_epsilon=.99,
                       max_epochs=3000)

# *** QUERY

print('Married(a) {}'.format(ltnw.ask('Married(a)')))
print('Worker(a) {}'.format(ltnw.ask('Worker(a)')))
print('Adult(a) {}'.format(ltnw.ask('Adult(a)')))
print('~Adult(a) {}'.format(ltnw.ask('~Adult(a)')))
# constant without rules
print('Worker(b) {}'.format(ltnw.ask('Worker(b)')))
print('Adult(b) {}'.format(ltnw.ask('Adult(b)')))
# constant with rules without informations
print('Worker(c) {}'.format(ltnw.ask('Worker(c)')))
print('Adult(c) {}'.format(ltnw.ask('Adult(c)')))
