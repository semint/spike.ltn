
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np

import tensorflow as tf

import logictensornetworks_wrapper as ltnw

#ltnw.constant('a',[1.,2.,3.,16])
#ltnw.constant('b',[1.,2.,3.,16])
ltnw.constant('a',[1, 15])
ltnw.constant('b',[1, 17])
ltnw.constant('c',[1, 18])
ltnw.constant('d',[1, 22])
ltnw.constant('e',[1, 99])
ltnw.constant('maggiorenne',[1, 18])

ltnw.constant('roma',[12.,41.])
ltnw.constant('frascati',[12.5,42.])
ltnw.constant('parigi',[48.,2.])


ltnw.function("eta", 1, fun_definition = lambda x:x[:,1])
ltnw.initialize_knowledgebase()
def _maggiorenne(x):
    return tf.cast(tf.less_equal(x,18),dtype=tf.float32)
def _crisp_leq(x,y):
    return tf.cast(tf.less_equal(x,y),dtype=tf.float32)
def _dist(x,y):
    return tf.exp(-tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(x,y)),axis=1,keepdims=True)))
ltnw.predicate("cleq",2,_crisp_leq)
ltnw.predicate("close",2,_dist)

print(ltnw.ask("cleq(eta(maggiorenne),eta(a))"))
print(ltnw.ask("cleq(eta(maggiorenne),eta(b))"))
print(ltnw.ask("cleq(eta(maggiorenne),eta(c))"))
print(ltnw.ask("cleq(eta(maggiorenne),eta(d))"))
print(ltnw.ask("cleq(eta(maggiorenne),eta(e))"))

ltnw.constant('z',[1, 12])
print("z {}".format(ltnw.ask("cleq(eta(maggiorenne),eta(z))")))

print(ltnw.ask('close(roma,parigi)'))
print(ltnw.ask('close(roma,frascati)'))
print(ltnw.ask('close(roma,roma)'))





