# -*- coding: utf-8 -*-
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np
import tensorflow as tf
import logictensornetworks_wrapper as ltnw

EMBEDDING_SIZE = 2
constants = list('abcdef')
print(constants)

for c in constants:
    ltnw.constant(c,min_value=[0.]*EMBEDDING_SIZE,max_value=[1.]*EMBEDDING_SIZE)

def simple_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE), stddev=0.1))
    def net(x):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(x,N),x),axis=1))
    return net

def double_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE*2, EMBEDDING_SIZE*2), stddev=0.1))
    def net(x,y):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(tf.concat([x,y],axis=1),N),tf.concat([x,y],axis=1)),axis=1))
    return net


ltnw.predicate("M",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("S",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("P",EMBEDDING_SIZE*2,pred_definition=double_net())
ltnw.predicate("L",EMBEDDING_SIZE,pred_definition=simple_net())

ltnw.variable('x',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))
ltnw.variable('y',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))
ltnw.variable('z',tf.concat([ltnw.CONSTANTS[c] for c in constants],axis=0))

ltnw.axiom('M(a)')
ltnw.axiom('~L(a)')
ltnw.axiom('S(a)')
ltnw.axiom('M(b)')
ltnw.axiom('L(b)')
ltnw.axiom('~S(b)')
ltnw.axiom('M(c)')
ltnw.axiom('L(c)')
ltnw.axiom('S(c)')
ltnw.axiom('M(d)')
ltnw.axiom('L(d)')
ltnw.axiom('~S(d)')
ltnw.axiom('~M(e)')
ltnw.axiom('~L(e)')
ltnw.axiom('~S(e)')
ltnw.axiom('~M(f)')
ltnw.axiom('~L(f)')
ltnw.axiom('S(f)')

def smooth_eq(x,y):
    return 1 - tf.truediv(tf.reduce_sum(tf.square(x - y), axis=1),
                          np.float32(EMBEDDING_SIZE)*tf.square(8.))

ltnw.predicate('eq',EMBEDDING_SIZE*2,
               pred_definition=smooth_eq)

ltnw.axiom ('forall x:(S(x) -> M(x))')
ltnw.axiom('forall x:(L(x) -> M(x))')
ltnw.axiom('forall x:(S(x) % exists y:P(x,y))')
ltnw.axiom('forall x:~P(x,x)')
ltnw.axiom('forall x,y:(P(x,y) -> P(y,x))')
ltnw.axiom('forall x,y,z:(P(x,y) & P(x,z) -> eq(y,z))')



ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(learning_rate=.01),
                              initial_sat_level_threshold=.1)
sat_level=ltnw.train(track_sat_levels=100,sat_level_epsilon=.99,max_epochs=4000)
constants = np.array(constants)
print('L(x) -> M(x)',constants[np.where(ltnw.ask('L(x) -> M(x)') < .95)[0].squeeze()])
print('S(x) -> M(x)',constants[np.where(ltnw.ask('S(x) -> M(x)') < .95)[0].squeeze()])

for c in constants:
    for p in ['L','S','M']:
        print("{}({})".format(p,c),ltnw.ask("{}({})".format(p,c)))
    print('L({}) -> M({})'.format(c,c),ltnw.ask('L({}) -> M({})'.format(c,c)))
    print('S({}) -> M({})'.format(c,c),ltnw.ask('S({}) -> M({})'.format(c,c)))

# x = ltnw.ask('x')
# plt.scatter(x[:,0],x[:,1])
# for i in range(len(constants)):
#     plt.annotate(constants[i],xy=x[i]+[.1,.1])
# plt.show()

for c in constants:
    for d in constants:
        query = 'P({},{})'.format(c,d)
        print(query,ltnw.ask(query))

for c in constants:
    query = 'S({}) % exists y:P({},y)'.format(c,c)
    print(query,ltnw.ask(query))