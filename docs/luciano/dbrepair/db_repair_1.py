# -*- coding: utf-8 -*-
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import logictensornetworks as ltn
ltn.LAYERS = 10
import logictensornetworks_wrapper as ltnw
ltnw.set_universal_aggreg('min')

EMBEDDING_SIZE = 6
constants = list('abcdef')
print(constants)

for c in constants:
    ltnw.constant(c,min_value=[0.]*EMBEDDING_SIZE,max_value=[1.]*EMBEDDING_SIZE)

def simple_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE), stddev=0.1))
    def net(x):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(x,N),x),axis=1))
    return net


ltnw.predicate("M",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("S",EMBEDDING_SIZE,pred_definition=simple_net())
ltnw.predicate("L",EMBEDDING_SIZE,pred_definition=simple_net())

ltnw.variable('x',tf.concat([ltnw.CONSTANTS[c] for c in constants if c not in ['b']],axis=0))
ltnw.axiom('~M(a)')
ltnw.axiom('~L(a)')
# ltnw.axiom('S(a)')
ltnw.axiom('~M(b)')
ltnw.axiom('L(b)')
ltnw.axiom('~S(b)')
ltnw.axiom('M(c)')
ltnw.axiom('L(c)')
ltnw.axiom('S(c)')
ltnw.axiom('M(d)')
ltnw.axiom('L(d)')
ltnw.axiom('~S(d)')
ltnw.axiom('~M(e)')
ltnw.axiom('~L(e)')
ltnw.axiom('~S(d)')
ltnw.axiom('~M(f)')
ltnw.axiom('L(f)')
ltnw.axiom('~S(f)')


ltnw.axiom ('forall x:(S(x) -> M(x))')
ltnw.axiom('forall x:(L(x) -> M(x))')



ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(learning_rate=.001),
                              initial_sat_level_threshold=.1)
sat_level=ltnw.train(track_sat_levels=100,sat_level_epsilon=.99,max_epochs=2000)
constants = np.array(constants)
print('L(x) -> M(x)',constants[np.where(ltnw.ask('L(x) -> M(x)') < 1.)[0].squeeze()])
print('S(x) -> M(x)',constants[np.where(ltnw.ask('S(x) -> M(x)') < 1.)[0].squeeze()])

for c in constants:
    for p in ['L','S','M']:
        print("{}({})".format(p,c),ltnw.ask("{}({})".format(p,c)))
    print('L({}) -> M({})'.format(c,c),ltnw.ask('L({}) -> M({})'.format(c,c)))
    print('S({}) -> M({})'.format(c,c),ltnw.ask('S({}) -> M({})'.format(c,c)))

