
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np

import tensorflow as tf

import logictensornetworks_wrapper as ltnw

ltnw.constant('roma',     [12.0, 41.])
ltnw.constant('frascati', [12.5, 42.])
ltnw.constant('parigi',   [48.0,  2.])


#ltnw.initialize_knowledgebase()

def _dist(x, y):
    return tf.exp(-tf.sqrt(
                    tf.reduce_sum(
                     tf.square(tf.subtract(x, y)), axis=1, keepdims=True)))

ltnw.predicate("close", 2, _dist)


print(ltnw.ask('close(roma,parigi)'))
print(ltnw.ask('close(roma,frascati)'))
print(ltnw.ask('close(roma,roma)'))





