
import logging; logging.basicConfig(level=logging.INFO)
import numpy as np
import tensorflow as tf
import logictensornetworks_wrapper as ltnw


# *** KNOWLEDGE

constants = list('abcde')

EMBEDDING_SIZE = 6
ltnw.constant('a', [99])
ltnw.constant('b', min_value=[50], max_value=[70])
ltnw.constant('c', [18])
ltnw.constant('d', [22])
ltnw.constant('e', [99])

#ltnw.constant('x')

ltnw.function("age", 1, fun_definition=lambda x: x[:, 0])


def outerCentenary():
    def _centenary(x):
        #return tf.cast(tf.less_equal(x, 18), dtype=tf.float32)
        a = tf.divide(tf.cast(tf.subtract(x, 100), dtype=tf.float32), 100)
        #print("A SHAPE: {}".format(a.shape))
        return a
    return _centenary


def _underage(x):
    return tf.cast(tf.less_equal(x, 18), dtype=tf.float32)

ltnw.predicate("underage", 2, _underage)
#ltnw.predicate("centenary", 2, _centenary)

ltnw.predicate("Aged", EMBEDDING_SIZE, pred_definition=outerCentenary())

ltnw.variable('x', tf.concat(
    [ltnw.CONSTANTS[c] for c in constants], axis=0))


ltnw.axiom('Aged(a)')

# *** TRAIN

ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(
                              learning_rate=.001),
                              initial_sat_level_threshold=.1)

sat_level = ltnw.train(track_sat_levels=100,
                       sat_level_epsilon=.99,
                       max_epochs=3000)

# *** QUERY

print('underage')
print(ltnw.ask('age(a)'))
print(ltnw.ask('Aged(a)'))
print(ltnw.ask('underage(age(a))'))
print(ltnw.ask('~underage(age(a))'))
#print(ltnw.ask('centenary(age(a))'))

print('adult')
print(ltnw.ask('age(d)'))
print(ltnw.ask('underage(age(d))'))
