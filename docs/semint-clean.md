# SemInt: Data Cleaning with LTNW

## Abstract

Processing clean data give as the ability to provide real information about reality.
Distinguishing from noise is an essential element in all data analysis disciplines.
Here we propose the development of a new algorithm for replacing incorrect or missing values based on learning with educated neural networks with a collection of clean data and domain rules expressed in fuzzy rules.
Providing the first practical results and proposing future research guidelines.

## The problem of data cleaning

In Data Science, cleaning up the data collected is one of the first steps in data processing to extract information.
A datum as a measure of a characteristic of reality, indicated in the literature as a feature, is selected and used by domain experts to interpret reality and make decisions. It therefore becomes essential to treat the input data in order to be able to filter the noise from the signal and successfully apply the transformations to provide a correct vision and interpretation.

For dirty data in this context we mean the antithesis of clean data, that is the misleading datum that falsifies reality.
Particularly the dirty data hide with noise those that are the real signals that deserve attention to indicate alarm situation or opportunity for new trends.

These errors can arise when entering new data, when merging existing archives but with schemes designed and maintained by separate groups,
in the phase of incorrect evolution of a single application on an archive. Silent errors can accumulate over time and make its correction much more expensive later, as they could cause the results of a report read by decision makers to deviate. It is therefore important to intercept the dirty data as soon as possible, both in ETL and streaming solutions.

There are different types of dirt and cleaning activities that can be applied, such as: [http://wp.sigmod.org/?p=2288] :

* missing values due to ignorance
* values out of range due to incorrect input
* violation of integrity constraints due to the union of several schemes
* duplicate entities for merge two tables with the same schema
* fragmented entities, identification of the unit

These errors can also interfere together in non-trivial ways. As indicated in [http://www.vldb.org/pvldb/vol9/p993-abedjan.pdf]
there is no silver ballet, no unique way to solve problems.

Main problems to be solved are:

1. provide a holistic solution to cleaning problems
2. scale to large data set
3. make interactive use an essential part


The data cleaning techniques derive experience gained from the Data Warehouseing field are different and numerous.
To provide a localization of the field of our interest we will refer to the scheme proposed in [DataCleaning-Ganti,Sarma-2013]()


The cleaning activities are:

* record matching, coupling semantically equivalent records with efficiency and accuracy to avoid insertion.
* deduplication, collecting records already entered representing the same logical entity, improving efficiency and quality of the final informative report.
* parsing, extract the appropriate data to insert into the rercord from the input data.

The approaches are divided into:

* ready-to-use domain specifics
* generic to the domain that require an extended configuration
* based on combinations of individual operators specialized in generic domain activity

Examples of generic operators:

* set similarity join using a variety of similarity functions for record matching
* clustering for deduplication
* regular expressions for parsing

These can be combined into scripts to provide complete and customized cleaning solutions.


## Our proposal

Our approach extends that based on operators:

* operators specialized in activities with enrichment of constraints from domain experts

Other works follow this approach in particular we follow the steps of the [HoloClean](http://holoclean.io/) cleanup framework which divides into detect and fix,
we propose our own solution of fix based on learning with neural networks and fuzzy logic rules.


Our activities:

* replacement or completion, proposing substitute values for absent or incorrect values in the records.

This activity is performed by an algorithm that involves a learning phase and then the application on production values with a constant access time.
The algorithm is able to search for optimal local solutions in the logical constraints of domain, time, pitch grain and sample data provided in configuration.
In the configuration of the algorithm the distribution of existing data will be used in the case of hypotheses of low-frequency dirty data or with a clean sample distribution to which reference is made enriched by constraints provided by the domain expert in the form of logical formulas. The alternative values proposed will therefore be respected for the statistical distribution rather than the constraints.

### LTWN: logica espressiva e adeguamento progressivo

What we propose here is an approach to our unique knowledge of neural network classifier enriched by fuzzy logic constraints.
Statistical Relational Learning (SRL) approaches have been developed for reasoning under uncertainty and learning in the presence of data and rich knowledge.

As presented in [Serafini](https://sites.google.com/fbk.eu/ltn/tutorial-ijcnn-2018) LogicTensor Networks (LTNs) are an SRL framework which integrates neural networks with first-order fuzzy logic to allow 
(i) efficient learning from noisy data in the presence of logical constraints, and 
(ii) reasoning with logical formulas describing general properties of the data.

 LTNs combine learning in deep networks with relational logical constraints.  I

LTN uses a First-order Logic (FOL) syntax interpreted in the real numbers, which is implemented as adeep tensor network.  Logical terms are interpreted as feature vectors in a real-valued n-dimensional space.  Function symbols are interpreted as real-valued functions, and predicate  symbols  as  fuzzy  logic  relations.   This  syntax  and  semantics,  called realsemantics, allow LTNs to learn efficiently in hybrid domains, where elements are com-posed of both numerical and relational information.


Being LTN a logic,  it provides a notion of logical consequence, which forms the basis for learning within LTNs, which is defined as best satisfiability, c.f. Section 4. Solving the best satisfiability problem amounts to finding the latent correlations that may exist between a relational background knowledge and numerical data attributes.   This formulation enables the specification of learning as reasoning, a unique characteristic of LTNs,

LTNs where the universally-quantified formulas are computed by using an aggregation operation, which avoids the need for instantiating all variables.

The semantics for connectives is defined according to fuzzy logic; using for instancethe Lukasiewicz t-norm2:

    G(¬φ) = 1−G(φ)
    G(φ∧ψ) = max(0,G(φ) +G(ψ)−1)
    G(φ∨ψ) = min(1,G(φ) +G(ψ))
    G(φ→ψ) = min(1,1−G(φ) +G(ψ))

Existential is eliminated from the language by introducing skolem functions.

Agrounded theory GTis a pair〈K,ˆG〉with a setKof closed formulasand a partial groundingˆG.
Differently from the classical satisfiability, when a GT is not satisfiable, we are interestedin the best possible satisfaction that we can reach with a grounding. 

Expression:

* logic of the first order
* fuzzy logic
* extension to arbitrary constraints

Progressive adjustment:

* approximation problem
* problems of neural networks: choice of algorithm, set of training data and embedding

### Advantages of blurred values

From certainty to approximation.
[Https://dizionarioautomazione.com/glossario/fuzzy-logic-logica-fuzzy/]
  It is an alternative logic to the Aristotelian one. In fact, it can be translated into Italian as "nuanced logic" because it rejects the principle of the excluded third of classical logic: if A is A, then A cannot be non-A. The fuzzy logic aims to reach the regulation of a system through the formalization of concepts derived from common experience.

The basic idea is that a quantity can assume not only boolean values (true / false), but a set of values indicating the level of "truth" of a certain expression. For this reason, fuzzy sets (fuzzy sets) are introduced on which "membership" functions are built with a generally triangular or trapezoidal shape.

Another benefit of the fuzzy logic is that it manages to handle the so-called gain-scheduling with linear combinations of known situations with extreme ease.


### LTWN and databases

We can use LTWN for the value prediction step to correct or calculate unknown values.

1. definition of space:
     construction of constants and variable ranges
2. choice of metrics:
     to be able to measure the distances between two features
3. characterization of the constraints in space:
     construction of the axioms
4. calculation of the unknown quantity:
     definition of the unknown
5. choice of optimization:
     train
6. interrogation of the space calculated by the train phase:
     ask

## Application examples

In order to understand how the translation steps of a table are carried out in LTWN, we propose a gradual series of practical examples accompanied by the respective differences that highlight.


### Minimal dependency example

Suppose a table with two tuples identified as t1, t2,
each tuple has two properties (A) Assumed, (B) Salary linked by a functional dependence A => B
the first tuple explicitly expressed both values as true.
the second tuple has explicitly expressed only the truth for A.
The test result for B (t2) will be true with rapid convergence, as there is no conflicting information.

Case study: [test_ldb_basic](../docker/docker-ldb/ldb/test/test_basic.txt)


### Minimal example of inconsistency

Starting from the previous categorical case if we add a description that contradicts a constraint, optimization will end by timeout and not by minimizing the distances of the constraints, leaving the value space in an unsecured configuration to satisfy all constraints.

Case study: [test_ldb_basic_contradiction](../docker/docker-ldb/ldb/test/test_basic_contradiction.txt)


### Minimal categorical example

To translate a categorical feature, the membership of the category must be specified for each value.
For example a person can take on value one of the possible values between (or married, or single, or divorced, or separated)
Mutual exclusion constraints must therefore be introduced for each possible category.
Given a certain training population we can ask LDB the value of a new person, according to the values
assumed by other parameters, eg worker, adult

Case study: [test_ldb_category](../docker/docker-ldb/ldb/sample/basic_category.py)


### Minimal numerical example

We translate a simple archive that describes a numerical feature.
Suppose a table (id: string, age: integer (0-100), adult: Boolean)
given a first sample of values we can use LDB as a classifier to make us suggest the best value:

Case study: [test_ldb_numeric](../docker/docker-ldb/ldb/sample/basic_numeric.py)

Choosing a two-dimensional metric
we can extend the constraints for example to a Euclidean space.

Case study: [test_ldb_numeric_ecludean](../docker/docker-ldb/ldb/sample/basic_numeric_euclidean.py)


### Minimal mixed example

We will mix numeric and categorical features. Given a numeric age feature and a categorial adult feature, we will try to compute che adult feature for constant 'c' given complete axiomatic definitions of constants 'a','b'. 


Case study: [test_ldb_mixed](../docker/docker-ldb/ldb/sample/basic_mixed.py)


### Distribution example

To understand how it affects the distribution of values let's start from a table in CSV format:

Case study: [test_ldb_105](../docker/docker-ldb/ldb/sample/basic_distrib.py)



### Hospital example

Assuming to start from values ​​not in enumerated format but free text values,
you will need to apply additional translation steps to the source data.
The first step is the construction of categories for each column.

We will use a subset of test dataset Hospital 100
Case study: [hospital_10](../docker/docker-ldb/ldb/sample/fix_hospital.py)

![hospital_10](../docker/docker-ldb/ldb/profile/memory_fix_hospital_10_1.png)

HoloClean hospital case study: [hospital_100](../docker/docker-ldb/ldb/sample/fix_hospital.py)

![hospital_100](../docker/docker-ldb/ldb/profile/memory_fix_hospital_100.png)

* highlighted mapping difficulty, to get good learning the whole record should be mapped to real vectors,
for some types of field this activity is not immediate.

Using the hospital data set 1000 and subsets consisting of:


HoloClean hospital case study: [hospital_200](../docker/docker-ldb/ldb/sample/fix_hospital.py)

![hospital_200](../docker/docker-ldb/ldb/profile/memory_fix_hospital_200.png)

HoloClean hospital case study: [hospital_500](../docker/docker-ldb/ldb/sample/fix_hospital.py)

![hospital_500](../docker/docker-ldb/ldb/profile/memory_fix_hospital_500.png)

HoloClean hospital case study: [hospital_750](../docker/docker-ldb/ldb/sample/fix_hospital.py) 

![hospital_750](../docker/docker-ldb/ldb/profile/memory_fix_hospital_750.png)

HoloClean hospital case study: [hospital_1000](../docker/docker-ldb/ldb/sample/fix_hospital.py) > 32Gb

![hospital_1000](../docker/docker-ldb/ldb/profile/memory_fix_hospital_1000.png)

* highlighted scalability difficulty, network training, application and effectiveness times remain largely optimizable.

Scalability is a necessary prerequisite for the commissioning of real cases.

To study, where possible, automatic construction techniques of a cardinality reduced synthetic distribution starting from the source data, such as the compression (clustering) of the data set without loss of information.


## Conclusions

Cleaning the data to be processed and analyzed is a priority and a challenge
given the different ways in which it can be soiled. To date there are no automated solutions.
Many are the proposals in search for challenges. Here we wanted to propose specifically a solution to the replacement of missing or incorrect values
based on blended learning with neural networks and fuzzy logic rules provided by a domain expert.
The experiments carried out indicate a positive applicability of the proposal, various optimization activities remain open.


### Future works

* Scalability, making training more efficient with more sophisticated networks, performance-oriented programming and effectively applying to the real set perhaps in streaming contexts.
* Interactivity, providing an incremental iterative collection module
* Resilience, evaluation of robustness to increase the load of injected errors of the data set:
  https://github.com/sjyk/datacleaning-benchmark
* Apply to examples of real production use.
* Extend to other types of data, such as images, sounds.


## References

data cleaning problem

* [Data cleaning is a machine learning problem that needs data systems help!](http://wp.sigmod.org/?p=2288)
* [Detecting Data Errors:Where are we and what needs to be done?](http://www.vldb.org/pvldb/vol9/p993-abedjan.pdf)
* [Ganti and Sarma - 2013 - Data Cleaning A Practical Perspective](https://www.amazon.it/Data-Cleaning-Perspective-Management-2013-09-01/dp/B01JXT5HYW?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duc01-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B01JXT5HYW)

data cleaning proposal

* [Trends in Cleaning Relational Data: Cosistency and Deduplications, Foundations and Trends in Databases 2015](https://cs.uwaterloo.ca/~ilyas/papers/IlyasFnTDB2015.pdf)
* [A formal framework for probabilistic unclean databases, ICDT, 2019](https://arxiv.org/pdf/1801.06750.pdf)
* [Fuzzy logic (Logica fuzzy)](https://dizionarioautomazione.com/glossario/fuzzy-logic-logica-fuzzy/)
* [Logic Tensor Networks: Deep Learning and Logical Reasoning from Data and Knowledge](https://www.researchgate.net/publication/303969790_Logic_Tensor_Networks_Deep_Learning_and_Logical_Reasoning_from_Data_and_Knowledge)


---------------------

