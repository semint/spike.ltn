# Spikes on Logic Tensor Networks (LTN)

Deep Learning and Logic Reasoning from Data and Knowledge.

[LTN Introducion Notebook](https://nbviewer.jupyter.org/urls/bitbucket.org/semint/spike.ltn/raw/master/docs/notebooks/ltn-introduction/ltn101.ipynb)

[Data reparing with LTNW](docs/semint-clean.md)

[LTN DB Repair Notebook](https://nbviewer.jupyter.org/urls/bitbucket.org/semint/spike.ltn/raw/master/docs/notebooks/ldb-introduction/db-reparing.ipynb)

[Usage with PIP](docs/usage-pip.md)

[Usage with Docker files](docker/usage.md)

## References

[Project Overview](docs/semint-overall.md)

[Neural-Symbolic Learning and Reasoning with Constraints](https://sites.google.com/fbk.eu/ltn/tutorial-ijcnn-2018)

[Code LTN Reference](https://github.com/logictensornetworks/logictensornetworks)

[Notebook markdown](https://github.com/aaren/notedown)
