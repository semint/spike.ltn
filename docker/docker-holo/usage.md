# HOLO  usage

to build

        cd docker-tensorflow
        make

        cd docker-holo-base
        make

        cd docker-holo
        make

to run

first start database

        sh ./docker-run-psql.sh
        docker start pghc

then start holoclean

        sh ./docker-run.sh
        :/holo# cd examples 
        :/holo/examples# ./start_example.sh 

to stop database

        docker stop pghc

to restart database

        docker start pghc

### details for network

nicola:docker-holo/ (master✗) $ ip addr show                                                                                     [17:34:10]
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s31f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 54:ee:75:c2:8c:ac brd ff:ff:ff:ff:ff:ff
3: wlp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether f4:8c:50:4f:e2:14 brd ff:ff:ff:ff:ff:ff
    inet 10.11.168.9/20 brd 10.11.175.255 scope global dynamic noprefixroute wlp4s0
       valid_lft 688516sec preferred_lft 688516sec
    inet6 fe80::fdfa:1814:3f27:4e60/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
5: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:0b:0d:be:87 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:bff:fe0d:be87/64 scope link 
       valid_lft forever preferred_lft forever
11: vethf8d0fd5@if10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 46:d3:46:28:23:f5 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::44d3:46ff:fe28:23f5/64 scope link 
       valid_lft forever preferred_lft forever




nicola:docker-holo/ (master✗) $ sudo docker network create --subnet=10.11.168.9/20 mynet124                                      [17:35:05]
a50c0c1da72ac6771618c7b8d36b983bdd191e056d01add451b368cfe593d0db




nicola:docker-holo/ (master✗) $ sudo docker run --name pghc --ip 10.11.168.9 --network mynet124 \                                [17:40:28]
    -e POSTGRES_DB=holo -e POSTGRES_USER=holocleanuser -e POSTGRES_PASSWORD=abcd1234 \
    -p 5432:5432 \
    -d postgres:11
203d2fe1ffdbffac5d3dae823a3317f6aae94e6aac02d919dc15ba94f5ed1224



### temp: for database connection

to find local postgres IP

        ip a 

look for docker IP

change session config into example .py default IP

        hc = holoclean.HoloClean(
                db_name='holo',
                db_host='172.17.0.1',

and make new version of docker-holo