# Docker-LDB - LogicDataBase - usage

to build

        cd docker-tensorflow
        make

        cd docker-ldb
        make

to run sample

        sh ./docker-runsh.sh
	python
	>>> import sample.basic_category

to run as interactive LDB functions

        sh ./docker-run.sh
	>>> import code.ldb 

to profile

  uncomment
