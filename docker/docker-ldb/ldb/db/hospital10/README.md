# README

from folder:

    spike.ltn/docker/docker-ldb/ldb

run as:

    python3 -m code.fix

result:

    _FALSE A0_Address1(t1_1) [0.00533904]
    _FALSE A0_HospitalName(t1_1) [0.00537708]
    _FALSE A0_ProviderNumber(t1_1) [0.00816476]
    _TRUE A1_Address1(t1_1) [0.9836015]
    _TRUE A1_HospitalName(t1_1) [0.98575693]
    _FALSE A1_ProviderNumber(t1_1) [0.00925385]
    _FALSE A2_Address1(t1_1) [0.02314793]
    _FALSE A2_HospitalName(t1_1) [0.02036517]
    _TRUE A2_ProviderNumber(t1_1) [0.73817116]
    _FALSE A3_Address1(t1_1) [0.0421541]
    _FALSE A3_HospitalName(t1_1) [0.03097667]
    _FALSE A3_ProviderNumber(t1_1) [0.0231655]
    _FALSE A4_Address1(t1_1) [0.05784502]
    _FALSE A4_HospitalName(t1_1) [0.03987439]
    _FALSE A4_ProviderNumber(t1_1) [0.02709814]
    _FALSE A5_ProviderNumber(t1_1) [0.07232658]

TRUE filtered result:

    _TRUE A1_Address1(t1_1) [0.9836015]
    _TRUE A1_HospitalName(t1_1) [0.98575693]
    _TRUE A2_ProviderNumber(t1_1) [0.73817116]


Value fixed as:

    A2_ProviderNumber = 10019
