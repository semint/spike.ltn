# LDB - LogicDataBase


* Desiderata:
Check conceptual logic constraints satisfaction over a target relational database table exposed as CSV. 


* Input Files:

    <db_path>/atoms.txt

Collection of single data propositions.

    <db_path>/formulas.txt

Collection of relations, as multiple data propositions and first-order logic skolemized formulas.

    <db_path>/ranges.txt

Collection of predicates variables with corresponding domain range.


    <db_path>/atomatrix.txt

For atomic matrix propositions syntax:

    <cardinality>, <const_root>, <propositions>

    <db_path>/predicates.txt

Comma separated list of predicates used in formulas


* Usage:

Run as a python shell lib :

        python3 

Import the library:

        import code.ldb

Read the logic database input files:

        ldb.read_db("<db_dir_path>")

Train the neural network:

        ldb.train()

Export a report file with consistency checks:

        ldb.report_checks()

[see test/test_constraint.txt](test/test_ldb_api.txt)


* Demos:

[see test/test_basic.txt](test/test_basic.txt)
[see test/test_contradiction.txt](test/test_contradiction.txt)
[see test/test_constraint.txt](test/test_constraint.txt)
[see test/test_constraint.txt](test/test_matrix.txt)

run with:

         python3 -m doctest test/test_basic.txt 


run fix from ldb directory with:

          python3 -m code.fix

----

* Experimental:

    #
    # MAIN MACROS
    #

    def try_basic():
        read_db("../db/metabasic/")
        train()
        ask_atoms()
        #  check_errors()
        # report_checks()


    def try_check_105():
        read_db("../db/matrixdb105/")
        train()
        ask_atoms_for("ax01")
        ask_atoms_for("a_01")
        # check_errors()


    def try_fix_105():
        read_db("../db/matrixdb105/")
        fix("ax01")
        ask_atoms_for("a_01")


    def try_basic_stat():
        for i in range(5):
            read_db("../db/metabasic/")
            train()
            print(check_vector())
            clear_db()


    def try_batch():
        report_fix("ax01", "../db/matrixdb105/", 2)


    def try_basic_2d():
        read_db("../db/metabasic2d/")
        fix("t1")
        ask_atoms()


    def try_ciclone():
        report_train("ax_1", dbpath="db/dedudb10/", reportpath="report/train_dedudb1_con_vincoli.csv", times=2)


    def try_dedudb():
        read_db('../db/dedudb10/')
        train()
        ask_atoms_for('ax_1')
        ask_atoms_for('a1_1')


* profiles memory

    $ pip install -U memory_profiler

    @profile
    def my_func():

    $ python -m memory_profiler example.py

    mprof run <executable>
    mprof plot

    mprof run: running an executable, recording memory usage
    mprof plot: plotting one the recorded memory usage (by default, the last one)
    mprof list: listing all recorded memory usage files in a user-friendly way.
    mprof clean: removing all recorded memory usage files.
    mprof rm: removing specific recorded memory usage files


* profiles cpu

    import cProfile
    import re
    cProfile.run('re.compile("foo|bar")')


ncalls

    for the number of calls,
tottime

    for the total time spent in the given function (and excluding time made in calls to sub-functions)
percall

    is the quotient of tottime divided by ncalls
cumtime

    is the cumulative time spent in this and all subfunctions (from invocation till exit). This figure is accurate even for recursive functions.
percall

    is the quotient of cumtime divided by primitive calls
filename:lineno(function)

    provides the respective data of each function

    python -m cProfile [-o output_file] [-s sort_order] myscript.py