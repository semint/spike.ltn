import logging; logging.basicConfig(level=logging.INFO)

import numpy as np

import tensorflow as tf

import code.logictensornetworks_wrapper as ltnw
import code.logictensornetworks as ltn

#  Hyper-parameters
ltn.LAYERS = 10
ltnw.set_universal_aggreg('min')
EMBEDDING_SIZE = 6

ltnw.constant('a',min_value=[1.,2.,3.,0.,1.,2.],max_value=[1.,2.,3.,100.,1.,2.])
ltnw.constant('b',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
ltnw.constant('c',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
#ltnw.constant('d',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
ltnw.constant('e',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
ltnw.constant('f',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
ltnw.constant('g',min_value=[4.,5.,6.,0.,1.,2.],max_value=[4.,5.,6.,100.,1.,2.])
ltnw.constant("ten",[10.])
ltnw.constant("twenty",[20.])
print(ltnw.CONSTANTS)

def _close_eta(x,y):
    return 1-tf.abs(x-y)/100.

def _adult(x):
    age = x[:,3]
    print("AGE============")
    if age>=18: 
        return 1. 
    else: 
        0.

ltnw.function('eta',4,fun_definition=lambda x:x[:,3])
ltnw.predicate("close_eta",2,_close_eta)
ltnw.predicate("adult",1,_adult)

ltnw.axiom("close_eta(eta(a),ten)")
ltnw.axiom("close_eta(eta(b),twenty)")
ltnw.axiom("close_eta(eta(b),eta(c))")
#ltnw.axiom("adult(eta(e))")

def simple_net():
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE),
                    stddev=0.1))

    def net(x):
        a = tf.sigmoid(tf.reduce_sum(
                          tf.multiply(tf.matmul(x, N), x), axis=1))
        #print("A SHAPE: {}".format(a.shape))
        return a
    return net

ltnw.predicate("Adult", EMBEDDING_SIZE, pred_definition=simple_net())
#ltnw.predicate("Married", EMBEDDING_SIZE, pred_definition=simple_net())
#ltnw.predicate("Worker", EMBEDDING_SIZE, pred_definition=simple_net())

constants = list('abcefg')

ltnw.variable('z', tf.concat(
    [ltnw.CONSTANTS[c] for c in constants], axis=0))

#ltnw.axiom('forall z:(Worker(z) -> Adult(z))')
#ltnw.axiom('forall z:(Married(z) -> Adult(z))')
ltnw.axiom('forall z:(adult(z) -> Adult(z))')


#ltnw.axiom('~Adult(a)')
#ltnw.axiom('Adult(b)')
#ltnw.axiom('Worker(d)')


ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(learning_rate=.01),
                              initial_sat_level_threshold=.4)
ltnw.train(max_epochs=20000)


print("eta(a)",ltnw.ask('eta(a)'))
print("eta(b)",ltnw.ask('eta(b)'))
print("eta(c)",ltnw.ask('eta(c)'))
print("eta(d)",ltnw.ask('eta(e)'))

print("adult(a)",ltnw.ask('Adult(a)'))
print("adult(b)",ltnw.ask('Adult(b)'))
print("adult(c)",ltnw.ask('Adult(c)'))
print("adult(d)",ltnw.ask('Adult(e)'))

