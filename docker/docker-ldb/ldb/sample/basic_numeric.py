
import logging; logging.basicConfig(level=logging.INFO)

import numpy as np

import tensorflow as tf

import code.logictensornetworks_wrapper as ltnw


ltnw.constant('a',min_value=[1.,2.,3.,0.],max_value=[1.,2.,3.,100.])
ltnw.constant('b',min_value=[4.,5.,6.,0.],max_value=[4.,5.,6.,100.])
ltnw.constant('c',min_value=[4.,5.,6.,0.],max_value=[4.,5.,6.,100.])
ltnw.constant("ten",[10.])
ltnw.constant("twenty",[20.])
print(ltnw.CONSTANTS)

def _close_eta(x,y):
    return 1-tf.abs(x-y)/100.


ltnw.function('eta',4,fun_definition=lambda x:x[:,3])
ltnw.predicate("close_eta",2,_close_eta)

ltnw.axiom("close_eta(eta(a),ten)")
ltnw.axiom("close_eta(eta(b),twenty)")
ltnw.axiom("close_eta(eta(b),eta(c))")
ltnw.initialize_knowledgebase(optimizer=tf.train.RMSPropOptimizer(learning_rate=.01),
                              initial_sat_level_threshold=.4)
ltnw.train(max_epochs=20000)
print("eta(a)",ltnw.ask('eta(a)'))
print("eta(b)",ltnw.ask('eta(b)'))
print("eta(c)",ltnw.ask('eta(c)'))

