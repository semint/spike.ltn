import pandas as pd
import code.ldb_write as ldb_write
import code.ldb as ldb


# ~/Workspaces/semint/spike.ltn/docker/docker-ldb/ldb
# mprof run sample/fix_hospital.py
# mprof plot

def ground_cell(df1, row_id, col_id, val_enums):
    print("Ground {},{}".format(row_id, col_id))
    key = df1.iloc[row_id][col_id]
    val_enums[col_id]


def ground(df1, row_id, working_columns, column_enum):
    print("FIXING row {}".format(row_id))
    for index, column in enumerate(working_columns):
        enum_pos = df1.iloc[row_id][index].rsplit('_', 1)[0]
        print("{},{},{}".format(row_id, df1.iloc[row_id][index], column_enum[index][int(enum_pos)]))
        # print(column_enum[index][int(enum_pos)])    


def categorize(df, working_columns, column_enum):
    # estrarre i valori di ciascuna colonna in una propria enumeration
    for index, column in enumerate(working_columns):
        column_enum[index] = df[column].unique()
        print(column_enum[index])
    # costruisce un dataframe ridotto
    df1 = df[working_columns]
    # per ogni riga sostituire il valore con la costante categorica
    consts = []
    for col_id in range(len(working_columns)):
        for index, enum_val in enumerate(column_enum[col_id]):
            consts.append('A{}_{}'.format(index, working_columns[col_id]))
        df1 = df1.replace(to_replace=column_enum[col_id], value=consts)
        # print("------------------CATALOGO---{}----------------".format(col_id))
        # print(df1)
        consts.clear()
    return df1


def remove_line_from_file(line_to_remove, filename):
    print("****************************** REMOVING TARGET PREDICATE")
    with open(filename, "r") as f:
        lines = f.readlines()
    with open(filename, "w") as f:
        for line in lines:
            if line.strip("\n") != line_to_remove:
                f.write(line)  


def fix_csv(filepath, filename, working_columns, cells_to_fix,
            target_predicate="A1_ProviderNumber",
            target_tuple="t1_1"):

    # lettura della tabella
    df = pd.read_csv(filepath + filename)
    # prepara lo spazio per le enum di ciascuna colonna di lavoro
    column_enum = [None] * len(working_columns)
    df1 = categorize(df, working_columns, column_enum)

    print("++++++++ catalogo")
    print(df1)
    # assiomatizza la tabella escludendo le celle da correggere
    #   per ogni valore categorico
    #   calcola il suo range: assegna la categorie al range composto da ID riga
    #   calcola la formula che lo descrive negando i valori alternativi
    #   aggiunge la formula
    print("++++++++ key enum")
    column_key_enum = [None] * len(working_columns)
    for i in range(len(working_columns)):
        column_key_enum[i] = (df1.iloc[:, i].unique())
        print(column_key_enum[i])

    print("++++++++ axioms")
    for en_id in range(len(working_columns)):
        for en_val in column_enum[en_id]:
            print(en_val)

    # crea gli intervalli per costante
    for col in range(len(working_columns)):
        for key in column_key_enum[col]:
            var_range = []
            for row in range(df1.shape[0]):
                if df1.loc[row][col] == key:
                    var_range.append("t{}".format(row))
            print(key)
            print(var_range)
            ldb.init_var_key_range(key, var_range)


    # crea il file db assiomatizzato
    ldb.write_db(filepath, df1)

    # creare il file formule di mutua esclusione sui predicati.
    ldb_write.write_formulas(filepath, column_key_enum)

    ldb.atomize_matrix(filepath)

    # rimuove informazioni per ogni cella da correggere in df1
    remove_line_from_file(target_predicate+","+target_tuple, filepath+'atoms.txt')

    # crea il file dei predicati con i predicati tartget: A1_ProviderNumber
    ldb_write.write_predicates(filepath, target_predicate)

    # carica il db con i vincoli
    ldb.read_db(filepath)

    # train     
    ldb.train() 
   
    # rimappare i valori corretti sui valori della tabella  
    # ground(df1, 0, working_columns, column_enum)
    # ground(df1, 1, working_columns, column_enum)
    # ground(df1, 2, working_columns, column_enum)

    # report_fix out_file
    # [tid,attribute,correct_val]
    # TO DO 
    ldb.ask_atoms_for(target_tuple)




#
# MAIN MACROS
#


def try_guess_movies_rate():
    filepath = 'db/movies/'
    filename = 'movies.csv'
    working_columns = ['UserID', 'State', 'Time', 'Movie', 'Vote']
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_Vote", "t1_1")


def try_fix_hospital_10():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital10/'
    # filename, is the file name with extension
    filename = 'hospital_10.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t1_1")


def try_fix_hospital_100():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital100/'
    # filename, is the file name with extension
    filename = 'hospital_100.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t1_1")


def try_fix_hospital_200():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital200/'
    # filename, is the file name with extension
    filename = 'hospital_200.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t13_1")


def try_fix_hospital_500():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital500/'
    # filename, is the file name with extension
    filename = 'hospital_500.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t13_1")


def try_fix_hospital_750():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital750/'
    # filename, is the file name with extension
    filename = 'hospital_750.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t13_1")


def try_fix_hospital_1000():
    # filepath, is absolute path to logic database
    filepath = 'db/hospital1000/'
    # filename, is the file name with extension
    filename = 'hospital-1000-refined.csv'
    # list of columns to care about in training
    working_columns = ['ProviderNumber', 'HospitalName', 'Address1']
    # list of couples (row,col) of cells to fix
    cells_to_fix = [[1, 1], [1, 3]]
    fix_csv(filepath, filename, working_columns, cells_to_fix,
            "A1_ProviderNumber", "t13_1")


try_fix_hospital_750()
