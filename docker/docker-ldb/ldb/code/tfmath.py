import numpy as np
import tensorflow as tf


def simple_net(EMBEDDING_SIZE=2):
    N = tf.Variable(tf.random_normal((EMBEDDING_SIZE, EMBEDDING_SIZE), stddev=0.1))

    def net(x):
        return tf.sigmoid(tf.reduce_sum(tf.multiply(tf.matmul(x, N), x), axis=1))
    return net


def double_net(EMBEDDING_SIZE=2):
    N = tf.Variable(tf.random_normal(
        (EMBEDDING_SIZE*2, EMBEDDING_SIZE*2), stddev=0.1))

    def net(x, y):
        return tf.sigmoid(
            tf.reduce_sum(
                tf.multiply(
                    tf.matmul(
                        tf.concat([x, y], axis=1), N),
                    tf.concat([x, y], axis=1)),
                axis=1)
            )
    return net


def smooth_eq(x, y, EMBEDDING_SIZE=2):
    return 1 - tf.truediv(tf.reduce_sum(tf.square(x - y), axis=1),
                          np.float32(EMBEDDING_SIZE)*tf.square(8.))
