from code.ldb_log import _log
import numpy as np

_log("MODULE WRITE LOADED.")


def write_db(path, frame):
    print("writing db {}".format(path))
    # completa con prima colonna pari alla cardinalita'
    frame.insert(0, 'Cardinality', '1')
    frame.insert(1, 'counter' , ['t{}'.format(i) for i in range(len(frame))])
    # completa con seconda colonna pari a ID tupla
    frame.to_csv(path+"atomatrix.txt", header=False, index=False)


def write_formulas(filepath, column_key_enum):
  print("*************** WRITE FORMULAS")
  with open(filepath+"formulas.txt", "w") as f:
    for enume in column_key_enum:
      index  = 0
      for key in enume:
        enume_complemento = np.delete(enume, index)
        index = index +1 
        formula = "forall x:({}(x) ->".format(key)
        index2 = 0 
        for key_complemento in enume_complemento:
          index2 = index2 +1 
          if index2 == 1:
            formula = formula + " ( ~{}(x)".format( key_complemento)
          else:
            formula = formula + "  & ~{}(x)".format( key_complemento)
        formula = formula + " ))\n"
        print(formula)
        f.write(formula)


def write_predicates(filepath, target_predicate):
  with open(filepath+"predicates.txt", "w") as f:
    f.write(target_predicate)