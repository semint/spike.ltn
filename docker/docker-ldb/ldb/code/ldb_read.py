import tensorflow as tf
import pandas as pd
import numpy as np
import math
import os
from code.ldb_log import _log

_log("MODULE READ LOADED.")


def read_matrix_write_atoms(metadb_path):
    outfile = open(metadb_path+"atoms.txt", "w+")
    matrix = pd.read_csv(metadb_path+'atomatrix.txt', header=None)
    for indexR, row in matrix.iterrows():
        cardinality = row[0] + 1
        for times in range(1, cardinality):
            constant = row[1].strip()
            predicates = row[2:].dropna()
            for predicate in predicates:
                predicate = predicate.strip()
                atom = "{},{}_{}\n".format(predicate, constant, times)
                # print(atom)
                outfile.write(atom)
    outfile.close()


def read_atoms(metadb_path):
    global atoms
    global constants
    global predicates
    atoms = pd.read_csv(metadb_path+'atoms.txt', 
                        header=None, 
                        skip_blank_lines=True)
    constants = atoms[1].unique()
    predicates = [k for k in atoms[0].unique() if '~' not in k]


def read_predicates(metadb_path):
    global predicates
    global predicates_pure
    if os.path.isfile(metadb_path+'predicates.txt'):
        predicates_csv = pd.read_csv(metadb_path+'predicates.txt', header=None, skip_blank_lines=True)
        predicates_pure = np.asarray(predicates_csv.ix[0])
        # print(predicates)
        # print(predicates_pure)
        predicates = np.union1d(predicates, predicates_pure)
    else:
        predicates_pure = np.asarray([])

def read_formulas(metadb_path):
    global formulas
    with open(metadb_path+'formulas.txt') as f:
        formulas = f.readlines()
    formulas = [k for k in formulas if '#' not in k]


def read_ranges(metadb_path):
    global ranges 
    global variables 
    ranges = pd.read_csv(metadb_path+'ranges.txt', header=None)
    variables = ranges[0].unique()


def read_from_files(metadb_path):
    read_atoms(metadb_path)
    read_predicates(metadb_path)
    read_formulas(metadb_path)
    read_ranges(metadb_path)


def read_from_matrix_files(metadb_path):
    read_matrix_write_atoms(metadb_path)
