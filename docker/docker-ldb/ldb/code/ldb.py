import logging; logging.basicConfig(level=logging.INFO)

import sys
import pandas as pd
import tensorflow as tf
import code.logictensornetworks_wrapper as ltnw
import numpy as np
import code.tfmath as tfmath
import code.ldb_read as ldb_read
import code.ldb_write as ldb_write
import code.ldb_print as ald_print
import code.ldb_report as ldb_report
import code.ldb_check as ldb_check
from code.ldb_log import _log 


atoms = []
predicates = []
predicates_pure = []
constants = []
formulas = []
ranges = []
variables = []
initialized = False

TRUE_WEIGHT_TRASHOLD = 0.7
FALSE_WEIGHT_TRASHOLD = 0.3


#
# CLEAR
#

def _clear_variables():
    del globals()['variables']
    del globals()['constants']
    del globals()['predicates']


def _clear_dataframes():
    del globals()['formulas']
    del globals()['ranges']
    del globals()['atoms']


def clear_db():
    global initialized
    if not initialized:
        return
    ltnw._reset()
    _clear_variables()
    _clear_dataframes()
    initialized = False


#
# INIT
#

def initialize(EMBEDDING_SIZE=6):
    _log("Initilizing elements.")
    global initialized
    _init_predicates(EMBEDDING_SIZE)
    _init_constants(EMBEDDING_SIZE)
    _init_variables()
    _init_atoms()
    _init_formulas()
    initialized = True
    _log("Initialized.")


def _init_variables():
    for index, r in ranges.iterrows():
        print("========== variable range {}".format(index))
        v = r[0].strip()
        print(v)
        v_consts = r.dropna().tolist()[1:]
        if len(v_consts) == 0:
            v_consts = constants
        concat_const = tf.concat(
            [ltnw.CONSTANTS[c] for c in v_consts], axis=0)
        print(v_consts)
        ltnw.variable(v, concat_const)


def _init_constants(EMBEDDING_SIZE):
    for c in constants:
        ltnw.constant(c, min_value=[0.] * EMBEDDING_SIZE, max_value=[1.] * EMBEDDING_SIZE)


def _init_predicates(EMBEDDING_SIZE=6):
    ltnw.predicate('eq', EMBEDDING_SIZE*2, pred_definition=tfmath.smooth_eq)
    for p in predicates:
        ltnw.predicate(p, EMBEDDING_SIZE, pred_definition=tfmath.simple_net(EMBEDDING_SIZE))


def _init_atoms():
    for x in atoms.values:
        print("{}({})".format(x[0], x[1]))
        ltnw.axiom("{}({})".format(x[0], x[1]))


def _init_formulas():
    for f in formulas:
        ltnw.axiom(f)


def init_var_key_range(key, var_range, EMBEDDING_SIZE=6):
    for c in var_range:
        ltnw.constant(c, min_value=[0.] * EMBEDDING_SIZE, max_value=[1.] * EMBEDDING_SIZE)
    concat_const = tf.concat(
        [ltnw.CONSTANTS[c] for c in var_range], axis=0)
    ltnw.variable("v_".format(key), concat_const)

#
# TRAIN
#


def train(EMBEDDING_SIZE=6, learning_rate=0.1):
    _log("PARAM embedding:"+str(EMBEDDING_SIZE))
    _log("PARAM learning_rate:"+str(learning_rate))
    _log("PARAM max_epochs:"+str(18000))
    if not initialized:
        initialize(EMBEDDING_SIZE)
    _log("Initializing knowledgebase.")
    optimizer = tf.train.RMSPropOptimizer(learning_rate)
    aggregator = lambda *x: 1/tf.reduce_mean(1/tf.concat(x,axis=0))
    ltnw.initialize_knowledgebase(optimizer,
        formula_aggregator = aggregator,
        initial_sat_level_threshold=.1)
    _log("Training.")
    sat_level = ltnw.train(
        track_sat_levels=5, 
        sat_level_epsilon=.99, 
        max_epochs=8000)
    _log("Trained.")


#
# ASK
#

def ask_atoms():
    for c in constants:
        print("for {}".format(c))
        ask_atoms_for(c)


def ask_atoms_for(constant):
        global TRUE_WEIGHT_TRASHOLD
        global FALSE_WEIGHT_TRASHOLD
        for p in predicates:
            weight = ltnw.ask("{}({})".format(p, constant))
            if weight > TRUE_WEIGHT_TRASHOLD:
                print("_TRUE {}({})".format(p, constant), weight)
            elif weight < FALSE_WEIGHT_TRASHOLD:
                print("_FALSE {}({})".format(p, constant), weight)
            else:
                print("_UNKNOWN {}({})".format(p, constant), weight)


def ask(query):
    print(ltnw.ask(query))


#
# ADD
#

def add_axiom(axiom_to_add):
    ltnw.axiom(axiom_to_add)


def add_constant(const_to_add, EMBEDDING_SIZE=6):
    constants.append(const_to_add)
    min_value = [0.]*EMBEDDING_SIZE
    max_value = [1.]*EMBEDDING_SIZE
    ltnw.constant(const_to_add, min_value, max_value)


def add_predicate(pred_to_add, EMBEDDING_SIZE=6):
    predicates.append(pred_to_add)    
    ltnw.predicate(pred_to_add, EMBEDDING_SIZE, pred_definition=tfmath.simple_net(EMBEDDING_SIZE))


def add_variable(var_to_add):
    variables.append(var_to_add)
    const_concat = tf.concat([ltnw.CONSTANTS[c] for c in constants], axis=0)
    ltnw.variable(var_to_add, const_concat)


#
# UTILS
#

def read_db(metadb_path="../db/metabasic/"):
    # clear_db()
    ldb_read.read_from_files(metadb_path)
    globals()['atoms'] = ldb_read.atoms
    globals()['constants'] = ldb_read.constants
    globals()['variables'] = ldb_read.variables
    globals()['predicates'] = ldb_read.predicates
    globals()['predicates_pure'] = ldb_read.predicates_pure
    globals()['ranges'] = ldb_read.ranges
    globals()['formulas'] = ldb_read.formulas


def write_db(path, axioms):
    ldb_write.write_db(path, axioms)


def atomize_matrix(metadb_path):
    ldb_read.read_matrix_write_atoms(metadb_path)


def print_db():
    ldb_print.print_obj("ATOMS", atoms)
    ldb_print.print_list("FORMULAS", formulas)
    ldb_print.print_list("PREDICATES", predicates)
    ldb_print.print_list("VARIABLES", variables)
    ldb_print.print_list("CONSTANTS", constants)


def check_db():
    ldb_check.check_alignment(atoms, constants, predicates)


def report_checks():
    ldb_check.report_atoms_csv(atoms, constants, predicates)


def check_errors():
    ldb_check.check_errors(constants, formulas)


def check_vector():
    return ldb_check.check_atoms_as_vector(atoms, constants, predicates)


def check_vector_for(consta):
    return ldb_check.check_atoms_as_vector_for(atoms, consta, predicates)


def report_train(consta_to_fix, 
                 dbpath="../db/metabasic/", 
                 reportpath="../report/train_matrix.csv", 
                 times=3):
    clear_db()
    read_db(dbpath)
    df = pd.DataFrame(columns=ldb_check.check_atoms_as_vector_header_for(consta_to_fix, predicates))
    for i in range(times):
        print(" ---------training {}".format(i))
        train()
        print(" ---------checking {}".format(i))
        print(check_vector_for(consta_to_fix))
        df.loc[i] = check_vector_for(consta_to_fix)
        print(" ---------clearing {}".format(i))
        clear_db()
        print(" ---------reading {}".format(i))
        read_db(dbpath)
    df.to_csv(reportpath, mode="a")
    return df


def explain(consta):
    ldb_check.explain(consta, formulas)

