import pandas as pd
import tensorflow as tf
import code.logictensornetworks_wrapper as ltnw
from code.ldb_log import _log 

_log("MODULE REPORT LOADED.")

TRUE_WEIGHT_TRASHOLD = 0.7
FALSE_WEIGHT_TRASHOLD = 0.3


def report_atoms_csv(atoms, constants, predicates):
    global TRUE_WEIGHT_TRASHOLD
    global FALSE_WEIGHT_TRASHOLD
    outfile = open("report_atoms.csv", "a")
    for c in constants:
        for p in predicates:
            weight = ltnw.ask("{}({})".format(p, c))
            if weight > TRUE_WEIGHT_TRASHOLD:
                if _isAtom(atoms, p, c):
                    _report(outfile,
                            "T,{}({})".format(p, c))
                else:
                    _report(outfile, 
                            "WT,{}({})".format(p, c))
            elif weight < FALSE_WEIGHT_TRASHOLD:
                if not _isAtom(atoms, p, c):
                    _report(outfile, 
                            "F,{}({})".format(p, c))
                else:
                    _report(outfile,
                            "WF,{}({})".format(p, c))
            else:
                if not _isAtom(atoms, p, c):
                    _report(outfile,
                            "U,{}({})".format(p, c))
                else:
                    _report(outfile, 
                            "WU,{}({})".format(p, c))
    outfile.close()


def report_atoms(atoms, constants, predicates):
    global TRUE_WEIGHT_TRASHOLD
    global FALSE_WEIGHT_TRASHOLD
    outfile = open("report_atoms.txt", "w")
    for c in constants:
        for p in predicates:
            weight = ltnw.ask("{}({})".format(p, c))
            if weight > TRUE_WEIGHT_TRASHOLD:
                if _isAtom(atoms, p, c):
                    _report(outfile,
                            "TRUE {}({}) weight={}".format(p, c, weight))
                else:
                    _report(outfile,
                            "WRONG TRUE: {}({})".format(p, c))
            elif weight < FALSE_WEIGHT_TRASHOLD:
                if not _isAtom(atoms, p, c):
                    _report(outfile,
                            "FALSE {}({}) weight={}".format(p, c, weight))
                else:
                    _report(outfile,
                            "WRONG FALSE: {}({})".format(p, c))
            else:
                if not _isAtom(atoms, p, c):
                    _report(outfile,
                            "UNKNOWN {}({}) weight={}".format(p, c, weight))
                else:
                    _report(outfile,
                            "WRONG UNKNOWN: {}({})".format(p, c))
    outfile.close()


def _report(outfile, text):
    outfile.write(text+"\n")


def _isAtom(atoms, p, c):
    return len(atoms[(atoms[0] == p) & (atoms[1] == c)]) == 1
