import datetime


def _log(text):
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("___[{}] {}".format(time, text))
