
import pandas as pd
import tensorflow as tf
import code.logictensornetworks_wrapper as ltnw
from code.ldb_log import _log 

_log("MODULE CHECK LOADED.")

TRUE_WEIGHT_TRASHOLD = 0.7
FALSE_WEIGHT_TRASHOLD = 0.3


def check_atoms(atoms, constants, predicates):
    global TRUE_WEIGHT_TRASHOLD
    global FALSE_WEIGHT_TRASHOLD
    for c in constants:
        print("--")
        for p in predicates:
            weight = ltnw.ask("{}({})".format(p, c))
            if weight > TRUE_WEIGHT_TRASHOLD:
                print("TRUE {}({})".format(p, c))
                # assert(_isAtom(atoms, p, c))
            elif weight < FALSE_WEIGHT_TRASHOLD:
                print("FALSE {}({})".format(p, c))
                # assert(not _isAtom(atoms, p, c))
            else:
                print("UNKNOWN {}({})".format(p, c))


def check_atoms_as_vector_header(constants, predicates):
    result = []
    for c in constants:
        for p in predicates:
                result = result + ["{}({})".format(p, c)]
    return result


def check_atoms_as_vector(atoms, constants, predicates):
    global TRUE_WEIGHT_TRASHOLD
    global FALSE_WEIGHT_TRASHOLD
    result = []
    for c in constants:
        for p in predicates:
            weight = ltnw.ask("{}({})".format(p, c))
            if weight > TRUE_WEIGHT_TRASHOLD:
                result = result + ["T"]
            elif weight < FALSE_WEIGHT_TRASHOLD:
                result = result + ["F"]
            else:
                result = result + ["U"]
    return result


def check_atoms_as_vector_header_for(consta, predicates):
    result = []
    for p in predicates:
            result = result + ["{}({})".format(p, consta)]
    return result


def check_atoms_as_vector_for(atoms, consta, predicates):
    global TRUE_WEIGHT_TRASHOLD
    global FALSE_WEIGHT_TRASHOLD
    result = []
    for p in predicates:
        weight = ltnw.ask("{}({})".format(p, consta))
        if weight > TRUE_WEIGHT_TRASHOLD:
            result = result + ["T"]
        elif weight < FALSE_WEIGHT_TRASHOLD:
            result = result + ["F"]
        else:
            result = result + ["U"]
    return result


def check_alignment(atoms, constants, predicates):
    check_atoms(atoms, constants, predicates)


def _isAtom(atoms, p, c):
    return len(atoms[(atoms[0] == p) & (atoms[1] == c)]) == 1


def check_errors(constants, formulas):
    print("checking errors:")
    for c in constants:
        weight = ltnw.ask("ERR({})".format(c))
        if weight > TRUE_WEIGHT_TRASHOLD:
            print("ERROR ({})".format(c))
            explain(c, formulas)
    print("errors checked")


def explain(c, formulas):
    print("explain:")
    for f in formulas:
        f_const = f[9:].replace("x", c)
        weight = ltnw.ask(f_const)
        if weight < TRUE_WEIGHT_TRASHOLD:
            print("{}:{}".format(c, f_const))
